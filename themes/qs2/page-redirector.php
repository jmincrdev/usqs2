<?php  
/* Template Name: Course Redirector */

# this page template performs look-ups to put users into a course, and then redirect them to a 
# required lesson.  We have to use JavaScript for the actual redirect, so that we can in turn
# leverage HTML5 WebStorage.
#
# The page template saves course HTML5 storage data into the transient cache for 7 days.  This provides a cleaner, faster user-experience.

# testing:
# course=accept-payments
# lesson=automate-payments // paying-payroll-taxes
# http://usquickstart.staging.kinsta.com/email-tool/?course=accept-payments&lesson=automate-payments

# https://usqs2.qbcontent.com/email-tool/?course=accept-payments&lesson=automate-payments&cid=123


# http://staging.usqsnomad.kinsta.com/email-tool/?course=accept-payments&lesson=automate-payments&cid=123

# Ref: https://css-tricks.com/the-deal-with-wordpress-transients/

# our page-level variables
$courseLink = isset($_GET["course"]) ? htmlspecialchars($_GET["course"]) : null;
$lessonLink = isset($_GET["lesson"]) ? htmlspecialchars($_GET["lesson"]) : null;
$cid = isset($_GET["cid"]) ? htmlspecialchars($_GET["cid"]) : null;

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<script>
	var lCS = `
		<?php echo $redirectData['sideContent'];?>
	`;
		var rR = false;
		if(typeof(Storage) !== "undefined") {
			
			localStorage.setItem("course-slug", "<?php echo $courseLink;?>");
			
			rR = true;
		} else {
			// Sorry! No Web Storage support..
			console.log("No web storage support");
			rR = true;
		}
		if(rR == true){
			// simulate HTTP redirect, rather than click via win.loc.href
			window.location.replace("<?php echo get_bloginfo('url') . '/'.	$lessonLink;?>?cid=<?php echo $cid;?>");
		}
	</script>
	
	<meta http-equiv="refresh" content="0; url=<?php echo get_bloginfo('url') . '/'. $lessonLink . '?cid=' . $cid;?>" />
	<?php wp_head(); ?>
	
</head>
<body>
<header>
	<?php
		$header_block = get_post(Option::get("un-global-setting-section", "un-theme-header"));
		echo ( $header_block ? apply_filters("the_content", $header_block->post_content) : "No header defined" );
	?>
</header>

<div class="container">
	<div class="row">
		<div class="col-xs-12">		
			<noscript>
				<h1>Your browser does not support JavaScript. Please <a href="<?php echo get_bloginfo('url') . '/'. $lessonLink;?>?cid=<?php echo $cid;?>">click here</a> to navigate to the intended page.</h1>
			</noscript>
			<?php the_content(); ?>
		</div>
	</div>
</div>

<?php
if (function_exists($function = 'un_child_theme_footer')) {
	if ($child_footer_block_id = $function()) {
		$child_footer_block = get_post( $child_footer_block_id );
		echo apply_filters("the_content", $child_footer_block->post_content);
	}
}
?>
<footer>
	<?php 
		$footer_block = get_post(Option::get("un-global-setting-section", "un-theme-footer"));
		echo ( $footer_block ? apply_filters("the_content", $footer_block->post_content) : "No footer defined");
	?>
</footer>
<?php wp_footer(); ?>
</body>
</html>