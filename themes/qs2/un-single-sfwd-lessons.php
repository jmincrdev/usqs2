<?php 

	$cid 			= $current_id;
	$categories 	= get_the_category($cid);
	$courses = get_posts(array(
		'post_type' => 'sfwd-courses',
		'meta_query' => array(
			array(
				'key' => 'bound_lessons',
				'value' => '"' . $current_id . '"',
				'compare' => 'LIKE'
			)
		)
	));
	foreach($courses as $course){
		if(++$i === count($courses)) {
			$sub_courses   .= $course->post_name;
		}else{
			$sub_courses   .= $course->post_name.',';
		}
	}	
	$course 		= get_post_meta($cid, 'course_parent', true)?get_post((get_post_meta($cid, 'course_parent', true))):NULL;
	$bound_lessons 	= $course?get_lessons_of_course($course->ID):NULL;
	$vid 			= get_field("video", $cid);
	$position		= 0;
	$vidID 			= "";

	// Translations
	$index_url 		= get_localization('qs-localization-tutorial', 'QS_TUTORIAL_INDEX_URL');
	$index_text 	= get_localization('qs-localization-tutorial', 'QS_TUTORIAL_ALL');
	$click_tutorial = get_localization('qs-localization-tutorial', 'QS_TUTORIAL_CLICK');		

	if($vid){
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $vid, $vidID);
	}
	
?>

<section id="single" class="clearfix sfwd-courses">
	<div class="container">
		<div class="row">
			<aside class="hidden-xs col-sm-4 col-md-2">
				<?php 
					if($courses){			
						echo do_shortcode('[sidebar-lesson courses='.$sub_courses.'/]');
					}else{	
						?>
						<div>
							<div class="avenir-demi pt10">
								<a href="<?php echo $index_url ?>">&lt; <?php echo $index_text ?></a>
							</div>
							<div class="mt35 inner active">
								<div class="border-bottom"><?php echo $click_tutorial ?></div>
							</div>
								<ul class="item-group">
									<li class="item clearfix active">
										<div class="count">1.</div>
										<div class="desc">
											<a href="<?php get_permalink($cid); ?>"><?php echo get_the_title($cid); ?></a>
										</div>
									</li>								
									<?php
										$counter = 1;
										$args	= ['post_type' => 'sfwd-lessons', 'orderby' => 'rand', 'posts_per_page' => '3', 'post__not_in' => [$cid] ];
										$lessons  = get_posts($args);
											foreach ( $lessons as $lesson ) { 
												$counter++; ?>
												<li class="item clearfix ">
													<div class="count"><?php echo $counter ?>.</div>
													<div class="desc">
														<a href="<?php echo get_permalink($lesson->ID); ?>" onclick='localStorage.setItem("course-slug", "<?php echo get_post(get_post_meta($lesson->ID, 'course_parent', true))->post_name ?>")'><?php echo get_the_title($lesson->ID); ?></a>
													</div>
												</li>
											<?php
										}
									?>
								</ul>
							</div>						
					<?php
					}
					?>
			</aside>
			<article class="col-xs-12 col-sm-7 col-md-8" itemscope itemtype="http://schema.org/VideoObject">
				<?php 
					if($courses){
						echo do_shortcode('[header-lesson courses='.$sub_courses.'/]');
					}
				?>
				<div class="body-course">
					<div id="headline">
						<h2 itemprop="name"><span id="lesson-id"><?php echo $course?'':'1. '; ?></span> <?php echo get_the_title($cid); ?></h2>
					</div>
					<meta itemprop="thumbnailURL" content="<?php echo wp_get_attachment_url( get_post_thumbnail_id($un_current_post->ID) );?>" />
					<meta itemprop="uploadDate" content="<?php echo the_date('Y-m-d');?>" />
					<?php if($vid){?>
						<meta itemprop="embedURL" content="http://www.youtube.com/watch?v=<?php echo $vid; ?>" />
						<div id="video">
							<iframe id="player" width="1075" height="590" src="https://www.youtube.com/embed/<?php echo $vid; ?>?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allowfullscreen></iframe>
						</div>
					<?php } ?>	
					<div id="lead" class="row">
						<div class="mt30 col-xs-12 col-md-7" itemprop="description">
							<?php echo get_field("intro", $cid); ?>
						</div>
						<div id="btnbox" class="mt30 col-xs-12 col-md-4 pull-right">
							<ul class="btn-group">
								<?php # check if the repeater field has rows of data
								if( have_rows('cta_button_group', $cid) ):
									while ( have_rows('cta_button_group', $cid) ) : the_row();
										$b_color = get_sub_field('button_color', $cid);
										$b_icon = get_sub_field('button_icon', $cid);	?>
										<li><a href="<?php the_sub_field('button_link', $cid); ?>" data-wa-link="<?php the_sub_field('site_cat_val', $cid);?>">
											<button class="qb-btn QB-green" style="background-color:<?php echo $b_color;?>;border-color:<?php echo $b_color;?>"><?php the_sub_field('button_title', $cid);?></button>
										</a></li>
									<?php endwhile;
								else :
									# no rows found
								endif; ?>
							</ul>
						</div>
						<?php 
						# hide the content area for lessons with a video but no text.
						if( false === get_field("hide_content", $cid)){ ?>
							<!-- main article instructions -->
							<div id="instructions" class="col-xs-12 mt30">
								<h2 class="heading"><?php echo get_localization('qs-localization-lesson', 'QS_LESSON_SBS'); ?></h2>
								<?php echo $un_content; ?>
							</div>
						<?php }?>						
					</div>
				</div>
			</article>
		</div>
	</div>
</section>