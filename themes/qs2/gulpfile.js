'use strict';
let gulp = require("gulp");
let cleanCSS = require("gulp-clean-css");
let concat = require("gulp-concat");
let dedupe = require('gulp-dedupe');
let sass = require('gulp-sass');
let cssmin = require('gulp-cssmin');

gulp.task('sass', function () {
  return gulp.src(['./resources/assets/scss/**/*.scss', './resources/assets/scss/print.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('build.css'))
    //.pipe(cssmin())
    .pipe(cleanCSS({debug: true}, function(details){
        console.log(details);
    }))
    .pipe(dedupe())
    .pipe(gulp.dest('./resources/assets/css'));
});
