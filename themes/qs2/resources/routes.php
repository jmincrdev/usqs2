<?php 

/*----------------------------------------------------*/
// Detect CPT
/*----------------------------------------------------*/
Route::get('singular', ["sfwd-events", function($post, $query) {
	
	// get CPT category object
	$wp_object_term = wp_get_object_terms( get_the_ID(), 'events-category', array( 'fields' => 'ids' ) );
	if (is_array($wp_object_term)) {
		$cpt_category = current($wp_object_term);
	}

	// get category name
	if ($cpt_category) {
		$category = get_category($cpt_category);
		if (is_object($category) and $category->name)
		$cat_name=$category->name;
		$cat_slug=$category->slug;
	}

	return View::make('un-single-events', [
		"acf"=>get_fields(get_the_ID()),
		"category"=>$cat_name,
		"category_slug"=>$cat_slug,
		"wp_home"=>WP_HOME
	]);

}]);

Route::get('singular', ["sfwd-lessons", function($post, $query) {
	return View::make('un-single-sfwd-lessons', [
		'un_content' => un_render_shortcode($post->post_content),
		'un_no_menu' => true,
		'current_post' => $post
		/*
		'by' => $localization->get("un-sbc-by"), 
		'chapter' => $localization->get("un-sbc-chapter"), 
		'chapters' => $localization->get("un-sbc-chapters"), 
		'related_guides' => $localization->get("un-sbc-related-guides"),
		'read_more' => $localization->get("un-sbc-read-more"),
		'start_reading' => $localization->get("un-sbc-start-reading"),
		 * */
	]);
}]);

Route::get('tag', function($post, $query){
	
	$tag=get_query_var("tag");

	if ($tag) {

		// get page for pagination
		$page = sbc_get_page();

		$query = new WP_Query(	'orderby=post_date&order=DESC&tag='.$tag.'&post_status=publish'.
								'&posts_per_page=6'.
								'&paged='.$page ); 

		$tag_posts = $query->posts;
		$max_num_pages = $query->max_num_pages;
		
		return View::make('un-tag',
							[
								'tag' => $tag,
								'service_url' => UN_ROOT_DOMAIN . '/wp-json/un/v1/tag-lessons/',
								'tag_posts'=>$tag_posts, 
								'max_num_pages'=>$max_num_pages, 
								'page_as_parameter'=>true,
								
							]
		);

	}else{
		return View::make('404');
	}
	
	
	
});


/*----------------------------------------------------*/
// Events category routing for this theme
/*----------------------------------------------------*/

// Return true only if is a valid category
function is_events_category()
{
    return get_query_var( "events-category" );
}

// Add condition to check if we are in valid category 
add_filter('themosisRouteConditions', function($conds)
{
    $conds['is_events_category'] = 'is_events_category';
    return $conds;
});

// add events categories
Route::get('is_events_category', function () {
	
	$events_category=get_query_var( "events-category" );

	if ($events_category) {

		// find $events_category_id given the category slug
		$events_category = get_term_by('slug', $events_category, 'events-category');

		if (is_object($events_category) and $events_category->term_id) {

			return View::make('un-events-category-template', [
				"events_category_id"=>$events_category->term_id, 
				"events_category_name"=>$events_category->name, 
				"archive"=>-1 /* needed for header!? */
			]);

		} else {
			return View::make('404'); 
		}

	} else {
		return View::make('404'); 
	}

});
