<?php
/*
Plugin Name: Intuit Quickstart Lesson List
Author: rgonzalez
Version: 1.0
Description: Generate Lesson List with VC components
*/

function render_lesson_list($atts) {

	$out = "";

	# WP_Query arguments
	$args = array (
		'posts_per_page'		 => $atts['total'],
		'post_type'              => 'sfwd-lessons',
	);
	
	# if we have post IDs, lets use them in our query
	if($atts['ids']){
		$args['post__in'] = str_getcsv($atts['ids']);
	}

	$lessons='';

	$hb = new UNHandlebarsManager( __FILE__, plugins_url( 'hb_templates', __FILE__ ) );
	
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
	
		$css_mobile = 'col-xs-'.$atts['c_mobile'];
		$css_tablet = 'col-sm-'.$atts['c_tablet'];
		$css_desktop = 'col-lg-'.$atts['c_desktop'];
		
		while ( $query->have_posts() ) {
			$query->the_post();

			// condition for checking videos
			$course = get_post(get_post_meta(get_the_ID(), 'course_parent', true));

			$vid 	= get_field("video");
			if ($vid) {  // has video?
				$class_overlay	= "overlay";
			} else {
				$class_overlay	= "overlay2";
			}

			$lessons .= $hb->renderHB('qs-vc-lesson', [ 'css_mobile' 	=> $css_mobile,
														'css_tablet' 	=> $css_tablet,
														'css_desktop' 	=> $css_desktop,
														'thumbnail'		=> get_the_post_thumbnail(), 
														'permalink'		=> get_the_permalink(), 
														'class_overlay'	=> $class_overlay,
														'course'		=> $course->post_name,
														'title'			=> get_the_title(),
														'time'			=> qs_youtube_duration($post->ID),
														'yoast'			=> get_post_meta($post->ID, '_yoast_wpseo_metadesc', true)]);
			
		}

		$out .= $hb->renderHB('qs-vc-lesson-list', ['lessons'=>$lessons ]);

	}else{

	}
	wp_reset_postdata();

	return $out;
}

$qb_vc_lesson_list = new QB_VC_Component_Factory();

$qb_vc_lesson_list -> set(
	[
		"name"				=> "Intuit Quickstart Lessons List",
		"shortcode"			=> "qs_lesson_list",
		"template_file"		=> dirname(__FILE__).'/templates/list.php',
		"js"				=>	[
								["name"=>"qb-vc-lesson-list-js", "path" => "common_assets/js/lesson-list-2.js", "dependencies" => ['jquery']]
							],
		"category"			=> "QuickStart",
		"render_hook"		=> "render_lesson_list",
		"params"			=> // parameters, add params same as with any other content element
			[
				[
					"type"			=> "textfield",
					"heading"		=> __("Total items", "qs"),
					"param_name"	=> "total",
					"value"			=> 6,
					"description"	=> __( "Set max limit for items in grid or enter -1 to display all (limited to 1000).", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Lesson IDs", "qs"),
					"param_name" => "ids",
					"description" => __( "Enter the Lesson Post ID in CSV format (i.e. 11,42,77,99) ", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Mobile Columns", "qs"),
					"value"		=> 12,
					"param_name" => "c_mobile",
					"description" => __( "The number of columns an item will occupy in mobile views.", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Tablet Columns", "qs"),
					"value"		=> 6,
					"param_name" => "c_tablet",
					"description" => __( "The number of columns an item will occupy in tablet views.", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Desktop Columns", "qs"),
					"value"		=> 4,
					"param_name" => "c_desktop",
					"description" => __( "The number of columns an item will occupy in desktop views.", "qs" )
				],
			]
    ]
);

$qb_vc_lesson_list -> start();
