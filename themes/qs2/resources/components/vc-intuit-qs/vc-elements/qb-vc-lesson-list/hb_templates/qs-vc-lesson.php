<?php  if (!function_exists('lcr58ab38b24e530encq')) { function lcr58ab38b24e530encq($cx, $var) {
  if ($var instanceof LS) {
   return (string)$var;
  }

  return str_replace(array('=', '`', '&#039;'), array('&#x3D;', '&#x60;', '&#x27;'), htmlentities(lcr58ab38b24e530raw($cx, $var), ENT_QUOTES, 'UTF-8'));
 }}
 if (!function_exists('lcr58ab38b24e530ifvar')) { function lcr58ab38b24e530ifvar($cx, $v, $zero) {
  return ($v !== null) && ($v !== false) && ($zero || ($v !== 0) && ($v !== 0.0)) && ($v !== '') && (is_array($v) ? (count($v) > 0) : true);
 }}
 if (!function_exists('lcr58ab38b24e530raw')) { function lcr58ab38b24e530raw($cx, $v) {
  if ($v === true) {
   if ($cx['flags']['jstrue']) {
    return 'true';
   }
  }

  if (($v === false)) {
   if ($cx['flags']['jstrue']) {
    return 'false';
   }
  }

  if (is_array($v)) {
   if ($cx['flags']['jsobj']) {
    if (count(array_diff_key($v, array_keys(array_keys($v)))) > 0) {
     return '[object Object]';
    } else {
     $ret = array();
     foreach ($v as $k => $vv) {
      $ret[] = lcr58ab38b24e530raw($cx, $vv);
     }
     return join(',', $ret);
    }
   } else {
    return 'Array';
   }
  }

  return "$v";
 }}
if (!class_exists("LS")) {
class LS {
    public function __construct($str, $escape = false) {
        $this->string = $escape ? (($escape === 'encq') ? static::encq(static::$jsContext, $str) : static::enc(static::$jsContext, $str)) : $str;
    }
    public function __toString() {
        return $this->string;
    }
    public static function escapeTemplate($template) {
        return addcslashes(addcslashes($template, '\\'), "'");
    }
    public static function raw($cx, $v) {
        if ($v === true) {
            if ($cx['flags']['jstrue']) {
                return 'true';
            }
        }

        if (($v === false)) {
            if ($cx['flags']['jstrue']) {
                return 'false';
            }
        }

        if (is_array($v)) {
            if ($cx['flags']['jsobj']) {
                if (count(array_diff_key($v, array_keys(array_keys($v)))) > 0) {
                    return '[object Object]';
                } else {
                    $ret = array();
                    foreach ($v as $k => $vv) {
                        $ret[] = static::raw($cx, $vv);
                    }
                    return join(',', $ret);
                }
            } else {
                return 'Array';
            }
        }

        return "$v";
    }
    public static function enc($cx, $var) {
        return htmlentities(static::raw($cx, $var), ENT_QUOTES, 'UTF-8');
    }
    public static function encq($cx, $var) {
        return str_replace(array('=', '`', '&#039;'), array('&#x3D;', '&#x60;', '&#x27;'), htmlentities(static::raw($cx, $var), ENT_QUOTES, 'UTF-8'));
    }
}
}
return function ($in = null, $options = null) {
    $helpers = array();
    $partials = array();
    $cx = array(
        'flags' => array(
            'jstrue' => true,
            'jsobj' => true,
            'spvar' => true,
            'prop' => false,
            'method' => false,
            'lambda' => false,
            'mustlok' => false,
            'mustlam' => false,
            'echo' => true,
            'partnc' => false,
            'knohlp' => false,
            'debug' => isset($options['debug']) ? $options['debug'] : 1,
        ),
        'constants' =>  array(
            'DEBUG_ERROR_LOG' => 1,
            'DEBUG_ERROR_EXCEPTION' => 2,
            'DEBUG_TAGS' => 4,
            'DEBUG_TAGS_ANSI' => 12,
            'DEBUG_TAGS_HTML' => 20,
        ),
        'helpers' => isset($options['helpers']) ? array_merge($helpers, $options['helpers']) : $helpers,
        'partials' => isset($options['partials']) ? array_merge($partials, $options['partials']) : $partials,
        'scopes' => array(),
        'sp_vars' => isset($options['data']) ? array_merge(array('root' => $in), $options['data']) : array('root' => $in),
        'blparam' => array(),
        'partialid' => 0,
        'runtime' => '\LightnCandy\Runtime',
    );
    
    ob_start();echo '<div class="loopitem ',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['css_mobile'])) ? $in['css_mobile'] : null)),' ',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['css_tablet'])) ? $in['css_tablet'] : null)),' ',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['css_desktop'])) ? $in['css_desktop'] : null)),'">
';if (lcr58ab38b24e530ifvar($cx, ((is_array($in) && isset($in['thumbnail'])) ? $in['thumbnail'] : null), false)){echo '		<div class="thumb">
			<a href="',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['permalink'])) ? $in['permalink'] : null)),'"><div class="',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['class_overlay'])) ? $in['class_overlay'] : null)),'"></div>',lcr58ab38b24e530raw($cx, ((is_array($in) && isset($in['thumbnail'])) ? $in['thumbnail'] : null)),'</a>
		</div>
';}else{echo '';}echo '	<div class="meta">
		<h4><a href="',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['permalink'])) ? $in['permalink'] : null)),'" onClick="localStorage.setItem(\'course-slug\', \'',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['course'])) ? $in['course'] : null)),'\')">',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['title'])) ? $in['title'] : null)),' - ',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['time'])) ? $in['time'] : null)),'</a></h4>
';if (lcr58ab38b24e530ifvar($cx, ((is_array($in) && isset($in['yoast'])) ? $in['yoast'] : null), false)){echo '			<div class="excerpt">
				',lcr58ab38b24e530encq($cx, ((is_array($in) && isset($in['yoast'])) ? $in['yoast'] : null)),'
			</div>
';}else{echo '';}echo '	</div>
</div>
';return ob_get_clean();
};?>