<?php
/*
Plugin Name: Intuit Quickstart Course Card 2
Author: jmwebdev
Version: 1.0
Description: Generate Course Card 2 with VC component
*/

function render_course_card_2($atts) {

	$atts['course_id'] = (int) $atts['course_id'];
	$atts['num_lessons'] = (int) $atts['num_lessons'];

	if( is_int($atts['course_id']) ){
		
		#get the associated lessons IDs bound to a given course
		$post_ids = get_field("bound_lessons",$atts['course_id']);
		
		$temp = array();
		foreach($post_ids as $p){
			$temp[] = $p->ID;
		}
		$post_ids = str_getcsv($temp);
		
		# add the featured lesson to the BEGINNING of the query
		if($atts['featured_lesson'])	array_unshift($post_ids,(int)$atts['featured_lesson']);
		
		$args = array (
			'posts_per_page'=> ((int)$atts['num_lessons'] + 1), #add one for featured lesson
			'post__in'		=> $post_ids,
			'post_type'		=> array( 'sfwd-lessons' ), //sfwd-lessons
			'post_status'	=> array( 'publish' ),
		);
		$query = new WP_Query( $args );
		
		# get the return results into array
		$posts = (array) json_decode(json_encode($query->get_posts()), true);
		
		#var_dump($posts);die();
		
		# for clean code sake, here our are additional arrays to pass to HBS
		$course = $featured = array();
		
		# get the course info
		$course['heading'] = get_the_title($atts['course_id']);
		$course['see_all_text'] = $atts['see_all_text'];
		$course['see_all_link'] = get_the_permalink($atts['course_id']);

		# get the featured image/post setup, using the first one in our query
		$featured['title'] = $posts[0]['post_title'];
		$featured['link'] = get_permalink($posts[0]['ID']);
		$featured['img'] = wp_get_attachment_url( get_post_thumbnail_id($posts[0]['ID']) );			

		# remove the featured lesson from our query results
		array_shift($posts);
		
		
		# hack to remove handlebars from rendering system
		$out = '<div class="course-card-2">
					<div class="top">
						<a href="'.$featured['link'].'">
							<img src="'.$featured['img'].'" alt="'.$featured['title'].'" />
							<h4>'.$featured['title'].'</h4>
						</a>
					</div>
					<ul class="xoxo">';
		
		foreach($posts as $p){
			#var_dump($p);
			
		
			$out .= '<li><a href="'.get_permalink($p["ID"]).'">'.$p["post_title"].'</a></li>';
		}			
						
		$out .= '</ul>
					<a class="see-all" href="'.$course['see_all_link'].'">'.$course['see_all_text'].'</a>
			</div>';
	
		return $out;
		
		
		// create HB renderer with local paths
		#$hb = new UNHandlebarsManager( __FILE__ );
		#return $hb->renderHB('qs-vc-course-card-2', ['course'=>$course, 'featured'=>$featured, 'posts'=>$posts ]);
		
	}

}

$qb_vc_course_card_2 = new QB_VC_Component_Factory();

$qb_vc_course_card_2 -> set(
	[
		"name"				=> "Intuit Quickstart Course Card 2",
		"shortcode"			=> "qs_course_card_2",					
		"category"			=> "QuickStart",						
		"render_hook"		=> "render_course_card_2",
		"params"			=> // parameters, add params same as with any other content element
			[
				[
					"type" => "textfield",
					"heading" => __("Course ID", "qs"),
					"param_name" => "course_id",
					"description" => __( "Enter the course ID.", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Featured Lesson (Optional)", "qs"),
					"param_name" => "featured_lesson",
					"description" => __( "Enter the lesson ID you wish to feature, otherwise this will be automatically generated.", "qs" )
				],	
				[
					"type" => "textfield",
					"heading" => __("Number of Lessons to List", "qs"),
					"param_name" => "num_lessons",
					"description" => __( "How many Lessons should be listed below the Featured Lesson?", "qs" ),
					'value'         => __( '3' ),
				],
				[
					"type" => "textfield",
					"heading" => __("See All Text", "qs"),
					"param_name" => "see_all_text",
					"description" => __( "Localization for the 'See All' Link", "qs" ),
					'value'         => __( 'See All' ),
				],				
			]
    ]
);

$qb_vc_course_card_2 -> start();