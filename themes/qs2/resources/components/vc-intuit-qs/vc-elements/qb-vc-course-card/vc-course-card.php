<?php
/*
Plugin Name: Intuit Quickstart Course Card
Author: rgonzalez
Version: 1.0
Description: Generate Course Card with VC components
*/

function render_course_card($atts) {

	if($atts['course']!= null){

		$file_aux = dirname(dirname(__FILE__))."/qb-vc-course-cards/vc-course-cards.php";
		
		$hb = new UNHandlebarsManager( $file_aux, plugins_url( 'hb_templates', $file_aux ) );
		
		$post = (array) get_post($atts['course']);
		$post = get_post_extra_data($post);
		$post["num_lesson"] = count(get_field("bound_lessons",$post["ID"]));

		return $hb->renderHB('qs-vc-course-card', ['post'=>$post]);

	}

}

$qb_vc_course_card = new QB_VC_Component_Factory();

$qb_vc_course_card -> set(
	[
		"name"				=> "Intuit Quickstart Course Card",
		"shortcode"			=> "qs_course_card",
		"template_file"		=> dirname(__FILE__).'/templates/list.php',
		"category"			=> "QuickStart",						
		"render_hook"		=> "render_course_card",
		"params"			=> // parameters, add params same as with any other content element
			[
				[
					"type" => "textfield", // it will bind a textfield in WP
					"heading" => __("Course", "qs"),
					"param_name" => "course",
					"description" => __( "Enter the course ID (integer).", "qs" )
				],
			]
    ]
);

$qb_vc_course_card -> start();
