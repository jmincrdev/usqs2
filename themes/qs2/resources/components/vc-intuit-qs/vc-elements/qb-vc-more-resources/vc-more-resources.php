<?php

/*
Plugin Name: QS2 Home Page Component
Author: lisandrom
Version: 1.0
Description: This component includes the React Slider and the More resources block for home page
*/


function render_qs_more_resources($atts) {

    $more_resources = base64_encode(substr($atts["content"], 0, -2));
    $hb = new UNHandlebarsManager( __FILE__, plugins_url( 'hb_templates', __FILE__ ) ); // renderer of this VC
    $out = $hb->renderHB('qs-vc-more-resources-father11', [
        'job'               => $atts["job"], 
        'type'              => $atts["type"], 
        'the_content'       => $atts["the_content"], 
        'n'                 => $atts["n"], 
        'title'             => $atts["heading_title"], 
        'more_resources'    => $more_resources
    ]);
    return $out;

}

function render_qs_resource($atts) {

    $url = "";
    if ($atts["url"]) {
        $a = new SimpleXMLElement($atts["url"]);
        if (is_object($a)) {
            $url = $a['href'];
        }

    }

    $hb = new UNHandlebarsManager( __FILE__, plugins_url( 'hb_templates', __FILE__ ) ); // renderer of this VC
    
    return $hb->renderHB('qs-vc-more-resources-child9', [
        'the_string' => $url . '|' . $atts["image"] . '|' . $atts["title"] . '|' . $atts["description"]
    ]);

}


$vc_more_resources = new QB_VC_Component_Factory();

// Seacrh jobs...
$query = new WP_Query(array(
    'post_type' => 'job_type',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order'   => 'ASC'
));

$jobs = [];
foreach ($query->posts as $post) {
    $jobs[$post->post_title]=$post->ID;
}

$vc_more_resources -> set(
    [
        "name"          => "QS Home Page Block",
        "shortcode"     => "qs_homepage_block",
        "render_hook"   => "render_qs_more_resources",
        "params"        => [ // parameters, add params same as with any other content element
            [
              'type'        => 'dropdown',
              'heading'     => __('Default Job'),
              'param_name'  => 'job',
              'admin_label' => true,
              'value'       => $jobs,
              'description' => __('Select the Job that will appear selected as the default')
            ],
            [
              'type'        => 'dropdown',
              'heading'     => __('Type'),
              'param_name'  => 'type',
              'admin_label' => true,
              'value'       => [
                'Small Business'    => 'SMB',
                'Accountant'        => 'ACC',
              ],
              'description' => __('Select the lessons type ')
            ],
            [
              'type'        => 'dropdown',
              'heading'     => __('Content'),
              'param_name'  => 'the_content',
              'admin_label' => true,
              'value'       => [
                'Video'     => 'video',
                'Webinar'   => 'webinar',
              ],
              'description' => __('Select the lessons content')
            ],
            [
                "type" => "textfield",
                "heading" => __("Number of lessons per block"),
                "param_name" => "n",
                "description" => __( "Enter the number of videos per block of content" )
            ],
            [
                "type"          => "textfield",
                "heading"       => __("Header title"),
                "param_name"    => "heading_title",
                "description"   => __("Enter a Title")
            ],
        ],
        "children"          => [
            [
                "name"          => "Resource",
                "shortcode"     => "more_resources_resource",
                "render_hook"   => "render_qs_resource",
                "params"        => [
                    [
                        "type"          => "textfield",
                        "heading"       => "Title",
                        "param_name"    => "title",
                        "description"   => "Enter a title"
                    ],
                    [
                        "type"          => "textfield",
                        "heading"       => "Description",
                        "param_name"    => "description",
                        "description"   => "Enter a description"
                    ],            
                    [
                        "type"          => "vc_link",
                        "heading"       => "URL",
                        "param_name"    => "url",
                        "description"   => "Enter a URL"
                    ],
                    [
                        'type'          => 'attach_image',
                        'heading'       => 'Image',
                        'param_name'    => 'image',
                        'value'         => 'Default' ,
                        'description'   => 'Choose an image' 
                    ],

                ]
            ]
        ]
    ]
);

$vc_more_resources -> start();
