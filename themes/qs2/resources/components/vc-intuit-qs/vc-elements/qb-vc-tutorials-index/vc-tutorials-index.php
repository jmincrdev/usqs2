<?php
/*
Plugin Name: Intuit Quickstart Course Index
Author: lisandrom
Version: 1.0
Description: Generate Tutorials Index with VC components
*/

function render_tutorial_index($atts) {

	#get the custom taxonomy's terms
	$args = array(
		'hide_empty'	=> true,
		'exclude'		=> str_getcsv($atts['exclude']),
	);
	
	$taxonomies = get_terms( 'tasktax', $args );

	$hb = new UNHandlebarsManager( __FILE__, plugins_url( 'hb_templates', __FILE__ ) );
	
	#loop through each term and perform a subquery
	foreach ( $taxonomies as $taxonomy ){

		$lessons = [];

		$args = [
			'post_type' 		=> 'sfwd-lessons',
			'post_status'       => ['publish'],
			'tax_query' 		=> [[
											'taxonomy' => 'tasktax',
											'field' => 'slug',
											'terms' => $taxonomy->slug
								]]
		];

		$query = new WP_Query($args);
		#the query
		
		$posts = (array) json_decode(json_encode($query->get_posts()), true);
		foreach($posts as $post) {
			$post 		= get_post_extra_data($post);
			$lessons[] 	= $post;
		}
		$out .= $hb->renderHB('qs-vc-tutorial-index', ['taxonomy_name'=>$taxonomy->name, 'posts'=>$lessons ]);
		
	}

	return $out;

}

$qb_vc_tutorials_index = new QB_VC_Component_Factory();

$qb_vc_tutorials_index -> set(
	[
		"name"				=> "Intuit Quickstart Tutorials Index",
		"shortcode"			=> "qs_tutorials_index",
		"render_hook"		=> "render_tutorial_index",
		"category"			=> "QuickStart",
		"params"			=> // parameters, add params same as with any other content element
			[
				[
					"type" => "textfield",
					"heading" => __("Exclude Tutorials", "qs"),
					"param_name" => "exclude",
					"description" => __( "Enter the Tutorial Taxonomy ID (integer) you wish to hide.", "qs" )
				],
			]
    ]
);

$qb_vc_tutorials_index -> start();
