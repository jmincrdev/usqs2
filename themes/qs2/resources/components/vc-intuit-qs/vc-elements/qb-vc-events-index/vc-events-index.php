<?php
/*
Plugin Name: Intuit Quickstart Course Index
Author: rgonzalez
Version: 1.0
Description: Generate Events Index with VC components
*/

# https://schema.org/Event
function render_events_index($atts) {

	$taxObjs   = array();
	$tabs      = '';
  $slug      = '';
  $titles    = '';
  $filter    = $atts['active_cityfilter'];

	#build the terms out -- get the events-type Object for each 'slug'
	foreach(str_getcsv($atts['active_tabs']) as $tax){
		$taxObjs[] = get_term_by("slug", $tax, "events-type");
	}
  $subcategories  = get_terms( 'events-category', array('hide_empty' => false,) );
  $locations      = get_terms( 'events-location', array('hide_empty' => false,) );
  $events_taxs    = get_terms( 'events-type',     array('hide_empty' => false,) );

  $i = 0;
  foreach($subcategories as $subcategory){
      if(++$i === count($subcategories)) {
          $subCategoriesIds   .= $subcategory->term_id.',0';
          $subCategoriesTitle .= $subcategory->name.',other';
      }else{
          $subCategoriesIds   .= $subcategory->term_id.',';
          $subCategoriesTitle .= $subcategory->name.',';
      }
  }

  $i = 0;
  foreach($locations as $location){
      if(++$i === count($locations)) {
          $locationsids   .= $location->term_id;
          $locationstitle .= $location->name;
      }else{
          $locationsids   .= $location->term_id.',';
          $locationstitle .= $location->name.',';
      }
  }

	$i = 0;
	foreach($events_taxs as $type){
		if(++$i === count($events_taxs)) {
			$tabs    .= $type->term_id;
      $slug    .= $type->slug;
      $titles  .= $type->name;
		}else{
			$tabs    .= $type->term_id.',';
      $slug    .= $type->slug.',';
      $titles  .= $type->name.',';
		}
	}

	return '<div data-qb-tabs="'.$tabs.'" data-qb-slugs="'.$slug.'" data-qb-event-category="'.$atts["sub_category"].'" data-qb-titles="'.$titles.'" data-qb-subtitles="'.$subCategoriesTitle.'" data-qb-subids="'.$subCategoriesIds.'" data-qb-filter="'.$filter.'" data-qb-locationsid="'.$locationsids.'" data-qb-locationsname="'.$locationstitle.'" data data-qb-events-index></div>';

}


# we need to use this function so that we can effectively hook into WordPress at the correct time; after init
# this what maps the shortcode to VC and sets up the params
function qs_events_index_init(){

    $events_taxs = get_terms(
        array(
            'taxonomy' => 'events-type',
            'hide_empty' => false,
        ) 
    );
    $tmp = array();
    foreach($events_taxs as $tx) $tmp[$tx->name] = $tx->slug;
    $events_taxs = $tmp;
    unset($tmp);

    $subcategories  = get_terms( 'events-category', array('hide_empty' => false, 'order'=>'name') );

    $arr_sub[0] = "All categories";
    if (is_array($subcategories)) {
      foreach($subcategories as $sc) {
        $arr_sub[$sc->name] = $sc->term_id;
      }    
    }

    $qb_vc_events_index = new QB_VC_Component_Factory();
    $qb_vc_events_index -> set(
        [
            "name"              => "Intuit Quickstart Events Index",
            "shortcode"         => "qs_events_index",
            "category"          => "QuickStart",                        
            "render_hook"       => "render_events_index",
            "params"            => // parameters, add params same as with any other content element
                [
                     [
                      "type" => "checkbox",
                      "holder" => "div",
                      "class" => "",
                      "heading" => __( "Select tabs to display", "qs" ),
                      "param_name" => "active_tabs",
                      "value" => $events_taxs,
                      "description" => __( "Check the tabs you wish to display.", "qs" )
                    ],
                     [
                      "type" => "checkbox",
                      "holder" => "div",
                      "class" => "",
                      "heading" => __("Select tabs where display the city filter", "qs"),
                      "param_name" => "active_cityfilter",
                      "value" => $events_taxs,
                      "description" => __( "Check the tabs you wish display the city filter.", "qs" )
                    ],
                    [
                      'type'          => 'dropdown',
                      'holder'        => 'div',
                      'class'         => '',
                      "heading" => __("Choose a sub-category", "qs"),
                      "param_name" => "sub_category",
                      "value" => $arr_sub,
                      'save_always'   => true,
                      "description" => __( "If you want to limit results to only one category, choose one.", "qs" )
                    ],
                ]
        ]
    );

    $qb_vc_events_index -> start();
}
## IMPORTANT:  http://wordpress.stackexchange.com/questions/29150/why-is-my-working-custom-taxonomy-not-in-get-taxonomies-array
add_action("widgets_init","qs_events_index_init");