<?php
/*
Plugin Name: Intuit Quickstart Lesson Step
Author: rgonzalez
Version: 1.0
Description: Generate Lesson Step with VC components
*/
function validate_url( $url ) {

	if(preg_match('/content\/uploads/', $url)) {
    	return true;
	}
	return false;
}

function render_lesson_step($atts) {

	// image is defined but it doesn't exists? find the right URL using image slug
	if ($atts["image"] and !validate_url($atts["image"])) {
		$_header = get_posts('post_type=attachment&name='.basename($atts["image"]).'&posts_per_page=1&post_status=inherit');
		$header = $_header ? array_pop($_header) : null;  
		$atts["image"] = $header ? wp_get_attachment_url($header->ID) : '';
	}

	$hb = new UNHandlebarsManager( __FILE__, plugins_url( 'hb_templates', __FILE__ ) );

	$prefix = chr(rand(97,121));  
	$atts['uid'] = $prefix. uniqid();

	return $hb->renderHB('qs-vc-lesson', $atts);
}

$qb_vc_lesson_step = new QB_VC_Component_Factory();

$qb_vc_lesson_step -> set(
	[
		"name"			=> "Intuit Quickstart Lesson Step",
		"shortcode"		=> "qs_lesson_step",
		"category"		=> "QuickStart",
		"render_hook"	=> "render_lesson_step",
		"params"		=> // parameters, add params same as with any other content element
			[
				[
					'type'			=> 'checkbox', 
					'heading'		=> __("New Step?", "qs"),
					'param_name'	=> "show_step",
					'save_always'	=> true,
					'description'	=> __( "This is a new step that should have it's own number.", "qs" )
				],
				[
					'type'			=> "textfield", 
					'heading'		=> __("Heading", "qs"),
					'param_name'	=> "heading",
					'description'	=> __( "Enter the heading of the step.", "qs" )
				],
				[
					"type"			=> "textarea_html",
					"heading"		=> __("The written instructions", "qs"),
					"param_name"	=> "content",
					"description"	=> __( "Enter your content.", "qs" )
				],
				[
					"type"			=> "attach_image", 
					"heading"		=> __("Image", "qs"),
					"param_name"	=> "image",
					"description"	=> __( "Select an image", "qs" )
				],
			]
	]
);

$qb_vc_lesson_step -> start();
