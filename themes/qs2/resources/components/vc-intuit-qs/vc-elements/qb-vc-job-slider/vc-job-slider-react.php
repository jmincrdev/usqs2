<?php
/*
Plugin Name: Intuit Quickstart Job Slider for QS v2
Author: lisandrom
Version: 1.0
Description: Generate Job menu and Lessons grid with VC components and React
*/

function render_job_slider($atts) {

	$job = $atts['job'];
	$type = $atts['type'];
	$n = $atts['n'];
	if (!$n or !is_numeric($n)) {
		$n=6;
	}
	$content = $atts['content'];

	return '<div data-default-qb-job-id="'.$job.'" data-qb-content="'.$content.'" data-qb-type="'.$type.'" data-qb-n="'.$n.'" data-qb-jobs-slider></div>';

}

$job_slider = new QB_VC_Component_Factory();

$query = new WP_Query(array(
    'post_type' => 'job_type',
    'post_status' => 'publish',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order'   => 'ASC'
));

$jobs = [];
foreach ($query->posts as $post) {
	$jobs[$post->post_title]=$post->ID;
}

$job_slider -> set(
	[
		"name"				=> "Intuit Quickstart (version 2) Job Slider ",
		"shortcode"			=> "qs_job_slider",
		"js"				=>	null,
		"category"			=> "QuickStart",						
		"render_hook"		=> "render_job_slider",
		"params"			=> // parameters, add params same as with any other content element
			[
			    [
			      'type'        => 'dropdown',
			      'heading'     => __('Default Job'),
			      'param_name'  => 'job',
			      'admin_label' => true,
			      'value'       => $jobs,
			      'description' => __('Select the Job that will appear selected as the default')
			    ],
			    [
			      'type'        => 'dropdown',
			      'heading'     => __('Type'),
			      'param_name'  => 'type',
			      'admin_label' => true,
			      'value'       => [
					'Small Business'	=> 'SMB',
					'Accountant'		=> 'ACC',
			      ],
			      'description' => __('Select the lessons type ')
			    ],
			    [
			      'type'        => 'dropdown',
			      'heading'     => __('Content'),
			      'param_name'  => 'content',
			      'admin_label' => true,
			      'value'       => [
					'Video'		=> 'video',
					'Webinar'	=> 'webinar',
			      ],
			      'description' => __('Select the lessons content')
			    ],
				[
					"type" => "textfield",
					"heading" => __("Number of lessons per block"),
					"param_name" => "n",
					"description" => __( "Enter the number of videos per block of content" )
				],
			]
    ]
);

$job_slider -> start();
