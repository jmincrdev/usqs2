<?php
/*
Plugin Name: Intuit Quickstart Course Index
Author: rgonzalez
Version: 1.0
Description: Generate Course Index with VC components
*/

function render_course_index($atts) {

	#get the custom taxonomy's terms
	$args = array(
		'hide_empty'	=> true,
		'exclude'		=> str_getcsv($atts['exclude']),
	);
	
	$taxonomies = get_terms( 'coursetax', $args );

	$hb = new UNHandlebarsManager( __FILE__, plugins_url( 'hb_templates', __FILE__ ) ); // renderer of this VC
	
	$file_aux = dirname(dirname(__FILE__))."/qb-vc-course-cards/vc-course-cards.php";
	$hb2 = new UNHandlebarsManager( $file_aux, plugins_url( 'hb_templates', $file_aux ) );	// renderer of cards

	#loop through each term and perform a subquery
	foreach ( $taxonomies as $taxonomy ){

		#the query		
		$args = array(
			'post_type' 		=> array('sfwd-courses'),
			'post_status'       => array('publish'),
			#'posts_per_page' 	=> -1,
			#'caller_get_posts'	=> 1,
			#'coursetax'		=> $taxonomy->slug,
			'tax_query' 		=> array(
										array(
											'taxonomy' => 'coursetax',
											'field' => 'slug',
											'terms' => $taxonomy->slug,
										),
									),

		 );
		
		$query = new WP_Query($args);
		$posts = (array) json_decode(json_encode($query->get_posts()), true);

		// get permalink, thumbnail, etc
		$content = null;
		foreach($posts as $post) {
			$post = get_post_extra_data($post);
			$post["num_lesson"] = count(get_field("bound_lessons",$post["ID"]));
			$post['excerpt'] 	= get_field("excerpt",$post["ID"]);
			$content .= $hb2->renderHB('qs-vc-course-card', ['post'=>$post, 'showcols'=>true]);
		}
		
		
		$out .= $hb->renderHB('qs-vc-course-index', ['taxonomy_name'=>$taxonomy->name, 'content'=>$content ]);
		
	}

	return $out;

}

$qb_vc_course_index = new QB_VC_Component_Factory();

$qb_vc_course_index -> set(
	[
		"name"				=> "Intuit Quickstart Course Index",
		"shortcode"			=> "qs_course_index",
		"category"			=> "QuickStart",						
		"render_hook"		=> "render_course_index",
		"params"			=> // parameters, add params same as with any other content element
			[
				[
					"type" => "textfield",
					"heading" => __("Exclude Course Categories", "qs"),
					"param_name" => "exclude",
					"description" => __( "Enter the Course Category ID (integer) you wish to hide.", "qs" )
				],
			]
    ]
);

$qb_vc_course_index -> start();
