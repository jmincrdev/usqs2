<?php
/*
Plugin Name: Intuit Quickstart Course Slider React version
Author: rgonzalez
Version: 1.0
Description: Generate Course Slider with VC components and React
*/

function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function render_course_cards_react($atts) {

	$lg		= $atts['cards_lg']?$atts['cards_lg']:'5';
	$md 	= $atts['cards_md']?$atts['cards_md']:'3';
	$sm 	= $atts['cards_sm']?$atts['cards_sm']:'1';
	$ids 	= $atts['courses_ids'];
	$title 	= $atts['title'];

	if($atts['courses_ids']!= null){
		
		return '<div data-qb-id="'.slugify($title).'" data-qb-lg="'.$lg.'" data-qb-md="'.$md.'" data-qb-sm="'.$sm.'" data-qb-courses="'.$ids.'" data-qb-title="'.$title.'" data-qb-course-slider></div>';		

	}	

}

$qb_vc_course_cards_react = new QB_VC_Component_Factory();

$qb_vc_course_cards_react -> set(
	[
		"name"				=> "Intuit Quickstart Course Slider (v2)",
		"shortcode"			=> "qs_course_slider",
		"js"				=>	null,
		"css"				=>	[
								["name"=>"slick-styles", "path" => "common_assets/css/slick.css", "dependencies" => []],
							],							
		"category"			=> "QuickStart",						
		"render_hook"		=> "render_course_cards_react",
		"params"			=> // parameters, add params same as with any other content element
			[
				[
					"type" => "textfield",
					"heading" => __("Heading", "qs"),
					"param_name" => "title",
					"description" => __( "Enter the Heading.", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Courses", "qs"),
					"param_name" => "courses_ids",
					"description" => __( "Enter the courses IDs in CSV format (11,42,77,99).", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Slides on Desktop", "qs"),
					"param_name" => "cards_lg",
					"description" => __( "How many cards/slides are show on desktop?", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Slides on Tablet", "qs"),
					"param_name" => "cards_md",
					"description" => __( "How many cards/slides are show on tablet?", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Slides on Mobile", "qs"),
					"param_name" => "cards_sm",
					"description" => __( "How many cards/slides are show on mobile?", "qs" )
				],												
			]
    ]
);

$qb_vc_course_cards_react -> start();
