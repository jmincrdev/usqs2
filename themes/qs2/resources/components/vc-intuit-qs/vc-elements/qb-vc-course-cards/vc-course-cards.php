<?php
/*
Plugin Name: Intuit Quickstart Course Slider
Author: rgonzalez
Version: 1.0
Description: Generate Course Slider with VC components
*/

function render_course_cards($atts) {

	$lg	= $atts['cards_lg']?$atts['cards_lg']:'5';
	$md = $atts['cards_md']?$atts['cards_md']:'3';
	$sm = $atts['cards_sm']?$atts['cards_sm']:'1';

	if($atts['courses_ids']!= null){
		
		$atts['courses_ids'] = str_getcsv($atts['courses_ids']);
		
		$args = array (
			'post__in'		=> $atts['courses_ids'],
			'post_type'		=> array( 'sfwd-courses' ), //sfwd-lessons
			'post_status'	=> array( 'published' ),
		);
		$query = new WP_Query( $args );
		$posts = (array) json_decode(json_encode($query->get_posts()), true);


		// create HB renderer with local paths

		$hb = new UNHandlebarsManager( __FILE__, plugins_url( 'hb_templates', __FILE__ ) );
		
		$cards=null;
		// loop courses by original order
		foreach($atts['courses_ids'] as $post_id) {
			
			// get the post and render it
			
			$key = array_search($post_id, array_column($posts, 'ID'));
			
			if (array_key_exists($key, $posts)) {
				
				$post = $posts[$key];
				
				$post 				= get_post_extra_data($post);
				$post['excerpt'] 	= get_field("excerpt",$post["ID"]);
				$post["num_lesson"] = count(get_field("bound_lessons",$post["ID"]));
				$cards .= $hb->renderHB('qs-vc-course-card', ['post'=>$post, 'showcols'=>true]);
				
			}
			
		}

		return $hb->renderHB('qs-vc-course-cards', ['cards'=>$cards, 'lg'=>$lg, 'md'=>$md, 'sm'=>$sm, 'title'=>$atts['title'], 'showcols'=>true]);
		
	}

}

$qb_vc_course_cards = new QB_VC_Component_Factory();

$qb_vc_course_cards -> set(
	[
		"name"				=> "Intuit Quickstart Course Slider",
		"shortcode"			=> "qs_course_slider",
		"js"				=>	[
								["name"=>"qb-vc-course-slider", "path" => "common_assets/js/course-carousel-2.js", "dependencies" => ['jquery']],
								["name"=>"slick", "path" => "common_assets/js/slick.min.js?cache=".rand(), "dependencies" => ['jquery']]
							],
		"css"				=>	[
								["name"=>"slick-styles", "path" => "common_assets/css/slick.css", "dependencies" => []],
							],							
		"category"			=> "QuickStart",						
		"render_hook"		=> "render_course_cards",
		"params"			=> // parameters, add params same as with any other content element
			[
				[
					"type" => "textfield",
					"heading" => __("Heading", "qs"),
					"param_name" => "title",
					"description" => __( "Enter the Heading.", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Courses", "qs"),
					"param_name" => "courses_ids",
					"description" => __( "Enter the courses IDs in CSV format (11,42,77,99).", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Slides on Desktop", "qs"),
					"param_name" => "cards_lg",
					"description" => __( "How many cards/slides are show on desktop?", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Slides on Tablet", "qs"),
					"param_name" => "cards_md",
					"description" => __( "How many cards/slides are show on tablet?", "qs" )
				],
				[
					"type" => "textfield",
					"heading" => __("Slides on Mobile", "qs"),
					"param_name" => "cards_sm",
					"description" => __( "How many cards/slides are show on mobile?", "qs" )
				],												
			]
    ]
);

$qb_vc_course_cards -> start();
