<?php  if (!function_exists('lcr590b960213ec4ifvar')) { function lcr590b960213ec4ifvar($cx, $v, $zero) {
  return ($v !== null) && ($v !== false) && ($zero || ($v !== 0) && ($v !== 0.0)) && ($v !== '') && (is_array($v) ? (count($v) > 0) : true);
 }}

 if (!function_exists('lcr590b960213ec4encq')) { function lcr590b960213ec4encq($cx, $var) {
  if ($var instanceof LS) {
   return (string)$var;
  }

  return str_replace(array('=', '`', '&#039;'), array('&#x3D;', '&#x60;', '&#x27;'), htmlspecialchars(lcr590b960213ec4raw($cx, $var), ENT_QUOTES, 'UTF-8'));
 }}

 if (!function_exists('lcr590b960213ec4raw')) { function lcr590b960213ec4raw($cx, $v, $ex = 0) {
  if ($ex) {
   return $v;
  }

  if ($v === true) {
   if ($cx['flags']['jstrue']) {
    return 'true';
   }
  }

  if (($v === false)) {
   if ($cx['flags']['jstrue']) {
    return 'false';
   }
  }

  if (is_array($v)) {
   if ($cx['flags']['jsobj']) {
    if (count(array_diff_key($v, array_keys(array_keys($v)))) > 0) {
     return '[object Object]';
    } else {
     $ret = array();
     foreach ($v as $k => $vv) {
      $ret[] = lcr590b960213ec4raw($cx, $vv);
     }
     return join(',', $ret);
    }
   } else {
    return 'Array';
   }
  }

  return "$v";
 }}

if (!class_exists("LS")) {
class LS {
 public static $jsContext = array (
  'flags' => 
  array (
    'jstrue' => 1,
    'jsobj' => 1,
  ),
);
    public function __construct($str, $escape = false) {
        $this->string = $escape ? (($escape === 'encq') ? static::encq(static::$jsContext, $str) : static::enc(static::$jsContext, $str)) : $str;
    }
    public function __toString() {
        return $this->string;
    }
    public static function stripExtendedComments($template) {
        return preg_replace(static::EXTENDED_COMMENT_SEARCH, '{{! }}', $template);
    }
    public static function escapeTemplate($template) {
        return addcslashes(addcslashes($template, '\\'), "'");
    }
    public static function raw($cx, $v, $ex = 0) {
        if ($ex) {
            return $v;
        }

        if ($v === true) {
            if ($cx['flags']['jstrue']) {
                return 'true';
            }
        }

        if (($v === false)) {
            if ($cx['flags']['jstrue']) {
                return 'false';
            }
        }

        if (is_array($v)) {
            if ($cx['flags']['jsobj']) {
                if (count(array_diff_key($v, array_keys(array_keys($v)))) > 0) {
                    return '[object Object]';
                } else {
                    $ret = array();
                    foreach ($v as $k => $vv) {
                        $ret[] = static::raw($cx, $vv);
                    }
                    return join(',', $ret);
                }
            } else {
                return 'Array';
            }
        }

        return "$v";
    }
    public static function enc($cx, $var) {
        return htmlspecialchars(static::raw($cx, $var), ENT_QUOTES, 'UTF-8');
    }
    public static function encq($cx, $var) {
        return str_replace(array('=', '`', '&#039;'), array('&#x3D;', '&#x60;', '&#x27;'), htmlspecialchars(static::raw($cx, $var), ENT_QUOTES, 'UTF-8'));
    }
}
}
return function ($in = null, $options = null) {
    $helpers = array();
    $partials = array();
    $cx = array(
        'flags' => array(
            'jstrue' => true,
            'jsobj' => true,
            'jslen' => true,
            'spvar' => true,
            'prop' => false,
            'method' => false,
            'lambda' => false,
            'mustlok' => false,
            'mustlam' => false,
            'echo' => true,
            'partnc' => false,
            'knohlp' => false,
            'debug' => isset($options['debug']) ? $options['debug'] : 1,
        ),
        'constants' =>  array(
            'DEBUG_ERROR_LOG' => 1,
            'DEBUG_ERROR_EXCEPTION' => 2,
            'DEBUG_TAGS' => 4,
            'DEBUG_TAGS_ANSI' => 12,
            'DEBUG_TAGS_HTML' => 20,
        ),
        'helpers' => isset($options['helpers']) ? array_merge($helpers, $options['helpers']) : $helpers,
        'partials' => isset($options['partials']) ? array_merge($partials, $options['partials']) : $partials,
        'scopes' => array(),
        'sp_vars' => isset($options['data']) ? array_merge(array('root' => $in), $options['data']) : array('root' => $in),
        'blparam' => array(),
        'partialid' => 0,
        'runtime' => '\LightnCandy\Runtime',
    );
    
    ob_start();echo '';if (lcr590b960213ec4ifvar($cx, ((is_array($in) && isset($in['showcols'])) ? $in['showcols'] : null), false)){echo '	<div class="item col-md-3 col-xs-6">
';}else{echo '';}echo '
	<div class="course-card">
';if (lcr590b960213ec4ifvar($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['thumbnail'])) ? $in['post']['thumbnail'] : null), false)){echo '			<div class="front" style="background-image:url(\'',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['thumbnail'])) ? $in['post']['thumbnail'] : null)),'\'); background-size: contain;"></div>
';}else{echo '';}echo '		<div class="back">
			<h3><a href="',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['permalink'])) ? $in['post']['permalink'] : null)),'">',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['post_title'])) ? $in['post']['post_title'] : null)),'</a></h3>
			<a class="course-info-xs hidden-lg hidden-md hidden-sm" href="',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['permalink'])) ? $in['post']['permalink'] : null)),'" onclick=\'localStorage.setItem("course-slug", "',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['post_name'])) ? $in['post']['post_name'] : null)),'");\'>',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['num_lesson'])) ? $in['post']['num_lesson'] : null)),' videos | view course ></a>
			<div class="fs-30"><a href="',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['permalink'])) ? $in['post']['permalink'] : null)),'">',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['num_lesson'])) ? $in['post']['num_lesson'] : null)),' videos</a></div>
			<div class="desc">',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['excerpt'])) ? $in['post']['excerpt'] : null)),'</div>
			<div class="bottom-link"><a href="',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['permalink'])) ? $in['post']['permalink'] : null)),'" onclick=\'localStorage.setItem("course-slug", "',lcr590b960213ec4encq($cx, ((isset($in['post']) && is_array($in['post']) && isset($in['post']['post_name'])) ? $in['post']['post_name'] : null)),'");\'>view course ></a></div>
		</div>
	</div>

';if (lcr590b960213ec4ifvar($cx, ((is_array($in) && isset($in['showcols'])) ? $in['showcols'] : null), false)){echo '	</div>
';}else{echo '';}echo '';return ob_get_clean();
};?>