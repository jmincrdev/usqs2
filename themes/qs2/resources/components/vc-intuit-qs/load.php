<?php
/*
Plugin Name: VC Intuit Components (Quickstart)
Description: Simple loader in order to load all VC QS Components
Author: Intuit
Version: 1.0
Author URI: http://www.intuit.com
*/

#Plugins loader
require_once(dirname(__FILE__).'/vc-elements/qb-vc-lesson-step/vc-lesson-step.php');
require_once(dirname(__FILE__).'/vc-elements/qb-vc-lesson-list/vc-lesson-list.php');

# Slider version 1 (slick.js)
// require_once(dirname(__FILE__).'/vc-elements/qb-vc-course-cards/vc-course-cards.php');
# Slider version 2 (react)
require_once(dirname(__FILE__).'/vc-elements/qb-vc-course-slider-react/vc-course-slider-react.php');
// require_once(dirname(__FILE__).'/vc-elements/qb-vc-job-slider/vc-job-slider-react.php');
require_once(dirname(__FILE__).'/vc-elements/qb-vc-more-resources/vc-more-resources.php');

require_once(dirname(__FILE__).'/vc-elements/qb-vc-course-card/vc-course-card.php');
require_once(dirname(__FILE__).'/vc-elements/qb-vc-course-card-2/vc-course-card-2.php');
require_once(dirname(__FILE__).'/vc-elements/qb-vc-course-index/vc-course-index.php');
require_once(dirname(__FILE__).'/vc-elements/qb-vc-events-index/vc-events-index.php');
require_once(dirname(__FILE__).'/vc-elements/qb-vc-tutorials-index/vc-tutorials-index.php');

if (file_exists(WPMU_PLUGIN_DIR.'/json-migrate-qs2/json-migrate-qs2.php')) {
	#json-migrate QS2
	require_once(WPMU_PLUGIN_DIR.'/json-migrate-qs2/json-migrate-qs2.php');
}
