@section('main')
<div id="main">
  	@include('un-qs-header')
	<div class="qb-wrapper">
	  <div class="qb-tutorial-page">
	    <div class="qb-tutorial-main">
			<div class="qb-background-gray">
				<div class="container">
					<div class="row">
	            		<div class="col-md-8">
            				<?php if($vid){?>
		                		<div id="video">
									<iframe id="player" width="100%" height="349" src="https://www.youtube.com/embed/<?php echo $vid; ?>?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>
								</div>
		                	<?php }else{ ?>
		            			<div class="image" style="background: url('{{$post_image}}'); background-size: cover;"></div>
		            		<?php } ?>
	              			<div class="qb-tutorial-content">              				              	
						        <div class="title">
									<p><?php echo get_field("intro", $cid); ?></p>			          
									<ul>
										<?php # check if the repeater field has rows of data
										if( have_rows('cta_button_group', $cid) ):
											while ( have_rows('cta_button_group', $cid) ) : the_row();
												$b_color = get_sub_field('button_color', $cid);
												$b_icon = get_sub_field('button_icon', $cid);	?>
												<li>
													<a href="<?php the_sub_field('button_link', $cid); ?>"
													data-wa-link="<?php the_sub_field('site_cat_val', $cid);?>"
													style="background-color:<?php echo $b_color;?>;border-color:<?php echo $b_color;?>">
														<?php the_sub_field('button_title', $cid);?>													
													</a>
												</li>
											<?php endwhile;
										else :
											# no rows found
										endif; ?>
									</ul>
						        </div>
						        <span id="qb-tutorial-toggle-button" href="#" class="qb-full-description">See more details <i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></span>
						        <div id="qb-tutorial-toggle" class="text toggle-close">
									<?php echo $un_content; ?>
						        </div>                
	              			</div>
	            		</div>
						<div class="qb-tutorial-sidebar col-md-4">
							<div class="qb-tutorial-list">
								<h3>More tutorials</h3>
								<ul>
									<li>
										<?php //print_r(un_get_lesson_related_posts()); ?>
										@if(count(un_get_lesson_related_posts()['posts']) > 0)
											@foreach(un_get_lesson_related_posts()['posts'] as $i => $post)
												<a href="{{ get_permalink($post->ID) }}">
													<div class="item">
														<div class="thumb">											
															<div class="overlay">
																<img class="play" src="http://res.cloudinary.com/dg6dch5ma/image/upload/v1498491211/play_sghjgs.png" />
															</div>
															<div class="preview" style="background: url({{get_the_post_thumbnail_url($post->ID)}}); background-size: cover;"></div>
														</div>
														<div class="meta">{{ $post->post_title }}</div>
													</div>
												</a>
											@endforeach
										@endif
									</li>
								</ul>
							</div>
							<div class="qb-tutorial-webinar">
								<?php
								
								if (isset($archive)) {
									$number = (int)$archive;
								} else {
									$number = get_the_ID();
								}
								 
								if ($number) {
									echo un_get_transient_lesson_banner($number);
								} else {
									echo un_get_transient_lesson_banner(0);
								}
								?>
							</div>
							<div class="qb-tutorial-tags">
				                <h4>Tags</h4>
				                <?php
								echo un_nucleus_tags(false);
								?>
							</div>
	        			</div>
	      			</div>
				</div>
	  		</div>
		</div>
	</div>

	<!-- The Modal -->
	<div class="qb-modal-component">
		<div id="myModal" class="modal">

			<!-- The Close Button -->
			<span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>

			<!-- Modal Content (The Image) -->
			<img class="modal-content" id="img01">
		</div>
	</div>

</div>
@stop