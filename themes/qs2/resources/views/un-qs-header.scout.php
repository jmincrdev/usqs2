<?php if (get_theme_version()=="version02") { ?>

	<?php if ( is_front_page()) { ?>
		@include('un-qs-header-home')	
	<?php } else { ?>
		<?php if ( is_search() ) { ?>
			@include('un-qs-header-search')
		<?php } else { ?>
			@include('un-qs-header-default')
		<?php } ?>
	<?php } ?>

<?php } ?>
