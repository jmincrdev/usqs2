{{-- includes main layout --}}
@extends('layouts.main')

@section('main')
  <div id="main">
  	
  	@include('un-qs-header')
		  	
	<?php
	$qs_events_index = '[vc_row][vc_column][qs_events_index active_tabs="webinar" sub_category="'.$events_category_id.'"][/vc_column][/vc_row]';
	echo apply_filters('the_content', $qs_events_index);
	?>

  </div>
@stop
