{{-- includes main layout --}}
@extends('layouts.main')

@section('main')
	@loop
		{{-- add the wrapper for rendered content --}}
		<div id="main">

		  	@include('un-qs-header')
		  	
			<div class="vc_row wpb_row vc_row-fluid container" style="margin: auto;">

			    <div class="wpb_column vc_column_container vc_col-sm-12">
			        <div class="vc_column-inner ">
			            <div class="wpb_wrapper">
			                <div>
			                    <div data-reactroot="" id="eventsTab">
			                        <div class="tab-content">
			                            <div class="active tab-pane row">
			                                <div class="col-xs-12 event">
			                                    <h3><a href="{{$wp_home}}?events-category={{$category_slug}}">{{$category}}</a></h3>
			                                    <div class="eventContainer row" itemscope="" itemtype="http://schema.org/Event">
			                                        <div class="col-xs-1 logo"><img src="http://usquickstart.qbcontent.com/content/themes/qs2/resources/assets/img/calendar.png">
			                                        </div>
			                                        <div class="col-xs-11">
			                                            <h3 itemprop="name">{{ get_field('custom_h1')?get_field('custom_h1'):Loop::title() }}</h3>
			                                            <div class="desc" itemprop="description">
			                                                <p>{{ Loop::content() }}</p>
			                                            </div>
			                                            <div class="meta row">
			                                                <div class="location col-md-6 col-xs-12">
			                                                    <div>
			                                                        <div class="dates">
			                                                            <p>{{$acf["event_repeater"][0]["location"]->name}}</p>
			                                                            <p>{{$acf["event_repeater"][0]["date"]}}</p>
			                                                            <p>{{$acf["event_repeater"][0]["start_time"]}} - {{$acf["event_repeater"][0]["end_time"]}} PST</p>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-6">
			                                                    <p>{{$acf["credits"]}} CPE Credits</p>
			                                                </div>
			                                            </div>
			                                            <div class="reserve"><a href="{{$acf["single_registration_link"]}}">Reserve your place</a>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>

		</div>
	@endloop
@stop
