@extends('layouts.main')
<?php 

	$cid 			= $current_id;
	$categories 	= get_the_category($cid);
	$courses = get_posts(array(
		'post_type' => 'sfwd-courses',
		'meta_query' => array(
			array(
				'key' => 'bound_lessons',
				'value' => '"' . $current_id . '"',
				'compare' => 'LIKE'
			)
		)
	));
	foreach($courses as $course){
		if(++$i === count($courses)) {
			$sub_courses   .= $course->post_name;
		}else{
			$sub_courses   .= $course->post_name.',';
		}
	}	
	$course 		= get_post_meta($cid, 'course_parent', true)?get_post((get_post_meta($cid, 'course_parent', true))):NULL;
	$bound_lessons 	= $course?get_lessons_of_course($course->ID):NULL;
	$vid 			= get_field("video", $cid);
	$position		= 0;
	$vidID 			= "";
	if(get_the_post_thumbnail_url($cid)){
		$post_image = get_the_post_thumbnail_url($cid);
	}else{
		$post_image = 'https://usquickstart.qbcontent.com/content/uploads/2016/10/Invoicing-in-QuickBooks.png';
	}
	$lesson_permalink = get_permalink($cid);
	$lesson_title = $current_post->post_title;

	// Translations
	$index_url 		= get_localization('qs-localization-tutorial', 'QS_TUTORIAL_INDEX_URL');
	$index_text 	= get_localization('qs-localization-tutorial', 'QS_TUTORIAL_ALL');
	$click_tutorial = get_localization('qs-localization-tutorial', 'QS_TUTORIAL_CLICK');		

	if($vid){
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $vid, $vidID);
	}
	
	//$vid=false;

?>

@include('un-single-sfwd-lessons-' . get_theme_version() )
