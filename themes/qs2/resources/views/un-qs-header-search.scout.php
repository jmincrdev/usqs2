  <div class="qb-wrapper">
    <div class="qb-search-page">
      
        <div class="container">

          <div class="row">
            <div class="qb-tutorial-header">
              <div class="col-md-12">
                <div class="qb-tutorial-back">
                  <?php if ( !is_front_page()) { ?>
                    <a href="{{WP_HOME}}"><i class="fa fa-chevron-left"></i> <?php echo un_site_name();?></a>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="qb-jobType-component">
              <div class="col-md-12 col-sm-12 hidden-xs">
                  <form action='{{WP_HOME}}/search/' method=GET>
                    <input id="sfilter" name="s" type="text" placeholder="Search QuickBooks Tutorial" value="<?php echo get_query_var("s", null); ?>" />
                    <i id="sfiltersubmit" class="fa fa-search"></i>
                  </form>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>

