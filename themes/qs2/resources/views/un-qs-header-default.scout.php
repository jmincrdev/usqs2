  <div class="qb-wrapper">

    <div class="qb-tutorial-page">
      <div class="qb-tutorial-header">
        <div class="container">

          <div class="row">
            <div class="col-md-12">
              <div class="qb-tutorial-back">
                <?php if ( !is_front_page()) { ?>
                  <a href="{{WP_HOME}}"><i class="fa fa-chevron-left"></i> <?php echo un_site_name();?></a>
                <?php } ?>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <?php if ( is_front_page()) { ?>
                <h1><?php echo un_site_name(); ?></h1>
              <?php } elseif(is_tag()) { ?>
                <h1><?php echo un_get_the_tag(); ?></h1>
              <?php } else { ?>
                <h1><?php echo get_the_title(); ?></h1>
              <?php } ?>
            </div>
            <div class="col-md-6 col-sm-6 hidden-xs">
              <div class="pull-right">
                <form action='{{WP_HOME}}/search/' method=GET>
                  <input id="filter" name='s' type="text" placeholder="Search" />
                  <i id="filtersubmit" class="fa fa-search"></i>
                </form>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>

