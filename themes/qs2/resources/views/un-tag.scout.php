@extends('layouts.main')
@section('main')

<div id="main">
  	@include('un-qs-header')
	<div class="qb-wrapper" style="padding-top: 30px;">
		<div class="qb-jobType-component">
			<div class="qb-background-gray">
				<div class="container">
					<div class="row">
						@foreach($tag_posts as $post)
							<div class="col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-6 col-md-4 col-lg-12">
								<div class="qb-jobType-video-item">
									<a href="{{ get_permalink($post->ID) }}">
										<div class="qb-jobType-video-thumb">
											<div class="overlay"></div>
											<div class="preview" style="background: url({{ get_the_post_thumbnail_url($post->ID) }}); background-size: cover;"></div>
										</div>
										<div class="qb-jobType-video-meta">
											<h4>{{ $post->post_title }}</h4>
											<span>{{ qs_youtube_duration($post->ID)  }}</span>
										</div>
									</a>
								</div>
							</div>
						@endforeach			
						<div id="show_more_div" class="col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-12 col-md-12 col-lg-12" @if($max_num_pages <= 1) style="display:none;" @endif>
							<div class="show_more_search tac"> Show more</div>
						</div>
					</div>
					<?php /*
	          		<div class="row">
	            		<div class="col-md-6 col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-6 col-lg-12">
	              			<div class="content">
	                			<h4>Related</h4>
				                {{ un_nucleus_tags(false) }}
	              			</div>
	            		</div>
	            	</div>
					*/ ?>
				</div>
			</div>
		</div>

<?php /*

		<div class="qb-tutorial-tags">
        	<div class="container">
          		<div class="row">
            		<div class="col-md-6 col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-6 col-lg-12">
              			<div class="content">
                			<h4>Related</h4>
			                {{ un_nucleus_tags(false) }}
              			</div>
            		</div>
          		</div>
        	</div>
      </div>

*/ ?>

	</div>
</div>
<script>

	var tag_pagination = {
		tag: '{{$tag}}',
		page : 1
	};

	$(document).ready(function(){
		
		$('.show_more_search').click(function(){
			var new_items = '';
			var new_item = '';
			
			tag_pagination.page++;
			
			$.get( "{{$service_url}}", tag_pagination, function( data ) {
			  	
			  	if(data.max_num_pages == tag_pagination.page){
			  		$('.show_more_search').hide();
			  	}
			  	
				$.each( data.posts, function( key, aPost ) {
				    var new_item = '';
					new_item += '<div class="col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-6 col-md-4 col-lg-12">';
					new_item += '<div class="qb-jobType-video-item">';
				    new_item += '<a href="'+ aPost.permalink + '">';
				    new_item += '<div class="qb-jobType-video-thumb">';
				    new_item += '<div class="overlay"></div>';
				    new_item += '<div class="preview" style="background: url('+ aPost.thumbnail + '); background-size: cover;"></div>';
				    new_item += '</div>';
				    new_item += '<div class="qb-jobType-video-meta">';
				    new_item += '<h4>'+ aPost.post_title + '</h4>';
				    new_item += '<span>'+ aPost.time + '</span>';
				    new_item += '</div>';
				    new_item += '</a>';
				    new_item += '</div>';
				    new_item += '</div>';
					new_items += new_item;
				});
				
				$(new_items).insertBefore( "#show_more_div" );
				
			});
		});
	});
</script>
@stop