@extends('layouts.main')

@section('main')
  <div id="main">

  <ul>
<?php
		foreach ($topic_posts as $post) {
			echo "<li><a href=\"" . get_permalink($post->ID) . "\">" . $post->post_title . "</a></li>";
		}
?>	
  </ul>

  </div>
@stop
