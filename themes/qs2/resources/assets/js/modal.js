var openModal = function(item) {
  
  var img = item;
  var modal = document.getElementById('myModal');
  var modalImg = document.getElementById("img01");

  img.onclick = function(){
      modal.style.display = "block";
      modalImg.src = item.src;
  }

  var span = document.getElementsByClassName("close")[0];
  
  span.onclick = function() { 
    modal.style.display = "none";
  }
};

var videoPost = () => {
  var thumbList = document.querySelectorAll('.to-video-modal img');

  thumbList.forEach(( item ) => {
    item.addEventListener('click', openModal(item), false);
  })
  
};

['DOMContentLoaded', 'ReactContentLoaded'].forEach( ( eventName ) => {
  
  var thumbList = document.querySelectorAll('.to-video-modal img');
  //document.removeEventListener(eventName , videoPost);
  
  if( thumbList ){
    document.addEventListener(eventName , videoPost );
  }
});