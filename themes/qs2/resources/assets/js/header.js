jQuery( document ).ready(function($){
  var button = $('#open-menu-btn');
  var ul = $('.subheader-mobile-menu');
  var inner_ul = $('.subheader-options');
  var holder = $('.mobile-subheader-menu-wrapper');
  var search_component = $('.animated-search');
  button.bind('click', function(){
     if (!$('.subheader-mobile-menu').hasClass('open')) {
        $('.animated-search').hide();
        holder.addClass('open');
        button.addClass('open');
        ul.addClass('open');
        inner_ul.slideDown("fast");
     } else {
        holder.removeClass('open');
        button.removeClass('open');
        ul.removeClass('open');
        inner_ul.slideUp("fast");
        $('.animated-search').show();
     }
  });
});