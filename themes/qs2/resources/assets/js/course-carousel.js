jQuery(function($){

    var $sliders = $(".inner");
    
    $(".qscarousel").each(function(){
        
        var $this = $(this);
        var slick = $this.find( $sliders ).slick({
			infinite: false,
			prevArrow: '<div class="prev-wrap"><button type="button" class="slick-prev">Previous</button></div>',
			nextArrow: '<div class="next-wrap"><button type="button" class="slick-next">Next</button></div>',
			slidesToShow: 5,
			slidesToScroll: 1,
			responsive: [
				{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 5,
					slidesToScroll: 1,
				  }
				},
				{
				  breakpoint: 600,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
			  ]
        });

	  $this.numTracks = $this.find('.slick-track').size();
	  $this.numActiveSlides = $this.find('.slick-track .slick-active').size();
	  $this.numTotalSlides = $this.find('.slick-track .slick-slide').size();
	  $this.numTotalSlides = $this.numTotalSlides / $this.numTracks;
	  $this.numActiveSlides = $this.numActiveSlides / $this.numTracks;
	  
	  //console.log("number of slides per track: "+$this.numTotalSlides);
	  //console.log("number of active slides per track: "+$this.numActiveSlides);
	  
	  $this.innerSlide = 0;
	  $this.outerSlide = $this.numActiveSlides;
	  
	  $this.find('.slick-active:nth-child('+$this.numActiveSlides+')').addClass("faded");
	  
		// On before slide change
		$this.on('beforeChange', function(event, slick, currentSlide, nextSlide){
		
			//console.log("Current: "+currentSlide);
			//console.log("Next: "+nextSlide);
			$this.innerSlide = nextSlide;
				
			// First lets take care of the old inner-most
			$this.find( ".slick-active" ).removeClass("faded");
			
			// add faded to new inner most
			if(parseInt(nextSlide) > 0){
				//$( ".slick-track .slick-slide:nth-child("+ (parseInt(innerSlide)+1) +")" ).addClass("faded");
				$this.find( ".slick-track .slick-slide:nth-child("+ (parseInt($this.innerSlide)+1) +")" ).addClass("faded");
			}
			
			// remve Old outer-most
			//$( ".slick-track .slick-slide:nth-child("+outerSlide+")" ).removeClass("faded");
			$this.find( ".slick-track .slick-slide:nth-child("+$this.outerSlide+")" ).removeClass("faded");
			
			// New outer-most
			$this.outerSlide = $this.innerSlide + $this.numActiveSlides;
			if($this.outerSlide < $this.numTotalSlides){
				$this.find( ".slick-track .slick-slide:nth-child("+$this.outerSlide+")" ).addClass("faded");
			}
			
			//console.log("New inner " +innerSlide);
			//console.log("New outer " +outerSlide);
			
		});
		
    });

  //var qsc = $('.qscarousel .inner').slick({
		
  //});
  
});
