<?php

function get_lessons_of_course($course_id){
	return get_field("bound_lessons",$course_id);
}


# get the YouTube video duration
# Please note that you'll need API_KEY to use YouTube API:
# 1) Create a new project here: https://console.developers.google.com/project
#	--> https://console.developers.google.com/home/dashboard?project=631702316427&authuser=1
# 2) Enable "YouTube Data API" under "APIs & auth"->APIs
# 3) Create new Server key under "APIs & auth"->Credentials
class YouTubeData{
    static $api_base = 'https://www.googleapis.com/youtube/v3/videos';
    static $thumbnail_base = 'https://i.ytimg.com/vi/';

    // $vid - video id in youtube
    // returns - video info
    public static function getVideoInfo($vid)
    {
        $params = array(
            'part' => 'contentDetails',
            'id' => $vid,
            'key' => QS_YOUTUBE_API_KEY,
        );

        $api_url = YouTubeData::$api_base . '?' . http_build_query($params);
        $result = json_decode(@file_get_contents($api_url), true);
		
        if(empty($result['items'][0]['contentDetails']))
            return null;
			
        $vinfo = $result['items'][0]['contentDetails'];

        $interval = new DateInterval($vinfo['duration']);
        $vinfo['duration_sec'] = $interval->h * 3600 + $interval->i * 60 + $interval->s;

        $vinfo['thumbnail']['default']       = self::$thumbnail_base . $vid . '/default.jpg';
        $vinfo['thumbnail']['mqDefault']     = self::$thumbnail_base . $vid . '/mqdefault.jpg';
        $vinfo['thumbnail']['hqDefault']     = self::$thumbnail_base . $vid . '/hqdefault.jpg';
        $vinfo['thumbnail']['sdDefault']     = self::$thumbnail_base . $vid . '/sddefault.jpg';
        $vinfo['thumbnail']['maxresDefault'] = self::$thumbnail_base . $vid . '/maxresdefault.jpg';

        return $vinfo;
    }
}

# format the time to be all purdy n stuff
# from: http://www.laughing-buddha.net/php/sec2hms
function qs_format_youtube_time($sec){

	$padHours=true;

    // start with a blank string
    $hms = "";
    
    // do the hours first: there are 3600 seconds in an hour, so if we divide
    // the total number of seconds by 3600 and throw away the remainder, we're
    // left with the number of hours in those seconds
    $hours = intval(intval($sec) / 3600); 

    // dividing the total seconds by 60 will give us the number of minutes
    // in total, but we're interested in *minutes past the hour* and to get
    // this, we have to divide by 60 again and then use the remainder
    $minutes = intval(($sec / 60) % 60); 
	
    // seconds past the minute are found by dividing the total number of seconds
    // by 60 and using the remainder
    $seconds = intval($sec % 60); 
	
    // add hours to $hms (with a leading 0 if asked for)
	if($hours>=1){
		$hms .= ($padHours) 
			  ? str_pad($hours, 2, "0", STR_PAD_LEFT). ":"
			  : $hours. ":";
		# add minutes with a leading "0"
		$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ":";
    }else{
		$hms .= $minutes . ":";
	}

    // add seconds to $hms (with a leading 0 if needed)
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT) . "s";

    // done!
    return $hms;
}

function qs_youtube_thumbnail($pid){

	# does teh value already exist?
	
	$out = get_post_meta( $pid, 'qs_task_yt_video_thumbnail', true );
  
	if( empty( $out ) ) {

		$yt = new YouTubeData();
		$ytEmbed = get_field("video", $pid);

		$vidID = "";
		if($ytEmbed){
			preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $ytEmbed, $vidID);
			
			$ytData = $yt->getVideoInfo($ytEmbed);

			$out = $ytData['thumbnail'];

			update_post_meta( $pid, 'qs_task_yt_video_thumbnail', $out );

		}else{
			$out = "";
		}

	}
	
	return $out;
}



# pulling it all together -- pull in the lesson WP ID and kick out the video duration
# saves the result to meta data -- will limit API queries and maximize page performance
function qs_youtube_duration($pid){

	# does teh value already exist?
	$out = get_post_meta( $pid, 'qs_task_yt_video_duration', true );
  
	if( ! empty( $out ) ) {
		# nothing to do here
	} else {
		$yt = new YouTubeData();
		$ytEmbed = get_field("video", $pid);
		$vidID = "";
		if($ytEmbed){
			preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $ytEmbed, $vidID);
			
			$ytData = $yt->getVideoInfo($ytEmbed);
			$out = qs_format_youtube_time($ytData['duration_sec']);
			update_post_meta( $pid, 'qs_task_yt_video_duration', $out );
			
		}else{
			$out = "";
		}
	}
	
	# if the video doesn't have a duration (aka error, missing, etc) do not show it
	return ( ($out != "0:00s") ? $out : "") ;
}

# return the slug or permalink of the first lesson of the given course
function get_first_lesson($course_id, $return_permalink=true) {
	
	# get the lessons in the course
	$bound_lessons = get_lessons_of_course($course_id);

	# update meta for use in lesson template
	update_post_meta( $bound_lessons[0], 'course_parent', $course_id );

	# get the permalink & remove swfd-lesson from permalink
	if ($return_permalink) {
		return get_the_permalink($bound_lessons[0]);
	# get the slug
	} else {
		return get_post_field( 'post_name', $bound_lessons[0] );
	}
	
}

# returns if a post uses a template in the routes/scout system of the child theme
function un_child_has_wp_template($post) {
	# if it is a post it doesnt use scout templates
	if ($post and is_object($post) and property_exists($post, 'post_type') and $post -> post_type == "post") {
		return false;
	# otherwise it uses a template
	} else {
		return true;
	}
}

# return the custom parent for routes in get_page_by_path endpoint
function un_child_custom_parent_react_routes() {
	return ["lessons", "courses"];
}

function get_related_posts($post_id = null)
{
	if(!$post_id){
		$post_id = get_the_ID();
	}
	$related_articles_ids	= get_field('custom_related_posts', $post_id);
	$related_articles		= [];
	foreach ($related_articles_ids as $article_id) {
		$related_articles[] = get_post($article_id);
	}

	return $related_articles;
}

function get_related_posts_plugin($post_id = null) {

	if(!$post_id){
		$post_id = get_the_ID();
	}
	
	$results_id = get_crp_posts_id(['postid' => $post_id, 'limit' => 3 ]);

	$results		= [];
	foreach ($results_id as $article) {
		$results[] = get_post($article->ID);
	}
	
	return $results;

}

function get_content_type($post_id = null, $slug)
{
	return 'contenty_type?';	
	if(!$post_id){
		$post_id = get_the_ID();
	}
	
	if ( ! empty( get_the_terms( $post_id, 'content_type_tax' ) ) ) {
		$term = get_the_terms( $post_id, 'content_type_tax' )[0];
	}else{
		$term = get_the_category( $post_id )[0];
	}
	
	if($slug)
		return $term ? $term->slug : null;
	
	return $term; 
}

function get_theme_version() {

	$theme_child_version = Option::get("un-global-setting-section", "un-theme-child-version");
	if($theme_child_version == ""){
		$theme_child_version = "version01";
	}
	return $theme_child_version;

}

// this is a hook to return an additional view to be included on the header
function un_view_child_hook() {
	return "un-qs-header";
}

// return an array with the selected menu for Small Business / Accountat links
function un_get_menu() {

	$menu = wp_get_nav_menu_items(Option::get("un-global-setting-section", "un-header-menu"));

	$arr_res = [];

	foreach ($menu as $item) {

		if (get_permalink(get_the_ID())==$item->url) {
			$active = true;
		} else {
			$active = false;
		}

		$arr_res[] = [
			"url"		=> $item->url,
			"title"		=> $item->title,
			"active"	=> $active,
		];

	}

	return $arr_res;

}

function un_site_name() {
	if (!$site_name = Option::get("un-global-setting-section", "un-site-name")) {
		$site_name = "QuickBooks Tutorials";
	} 
	return $site_name;
}



function un_get_the_tag() {

	if ($tag_slug = get_query_var("tag")) {
		$tag_obj = get_term_by('slug', $tag_slug, 'post_tag');
		if (is_object($tag_obj) and property_exists($tag_obj, 'name')) {
			return $tag_obj->name;
		}
	}

	return '';
}

