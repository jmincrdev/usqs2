<?php 

# COURSE TAXONOMY
function qs_course_taxonomy() {

	// Courses and course taxonomies will be available only in version 1
	if (un_child_version()==1) {

		$labels = array(
			'name'                       => _x( 'Course Categories', 'Taxonomy General Name', 'qs' ),
			'singular_name'              => _x( 'Course Category', 'Taxonomy Singular Name', 'qs' ),
			'menu_name'                  => __( 'Course Category', 'qs' ),
			'all_items'                  => __( 'All Categories', 'qs' ),
			'parent_item'                => __( 'Parent Category', 'qs' ),
			'parent_item_colon'          => __( 'Parent Category:', 'qs' ),
			'new_item_name'              => __( 'New CategoryName', 'qs' ),
			'add_new_item'               => __( 'Add New Category', 'qs' ),
			'edit_item'                  => __( 'Edit Category', 'qs' ),
			'update_item'                => __( 'Update Category', 'qs' ),
			'view_item'                  => __( 'View Category', 'qs' ),
			'separate_items_with_commas' => __( 'Separate Categories with commas', 'qs' ),
			'add_or_remove_items'        => __( 'Add or remove Categories', 'qs' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'qs' ),
			'popular_items'              => __( 'Popular Categories', 'qs' ),
			'search_items'               => __( 'Search Categories', 'qs' ),
			'not_found'                  => __( 'Not Found', 'qs' ),
			'items_list'                 => __( 'Categories list', 'qs' ),
			'items_list_navigation'      => __( 'Categories list navigation', 'qs' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => false,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'rewrite'                    => false,
			'show_in_rest'				 => true,
			'rest_base'					 => 'coursetax',
			'rest_controller_class'		 => 'WP_REST_Terms_Controller',		
		);
		register_taxonomy( 'coursetax', array( 'sfwd-courses' ), $args );

	}	
	
	## events
	$labels = array(
		'name'                       => _x( 'Event Type', 'Taxonomy General Name', 'qs' ),
		'singular_name'              => _x( 'Event Type', 'Taxonomy Singular Name', 'qs' ),
		'menu_name'                  => __( 'Event Type', 'qs' ),
		'all_items'                  => __( 'All Event Types', 'qs' ),
		'parent_item'                => __( 'Parent Event Type', 'qs' ),
		'parent_item_colon'          => __( 'Parent Event Type:', 'qs' ),
		'new_item_name'              => __( 'New Event TypeName', 'qs' ),
		'add_new_item'               => __( 'Add New Event Type', 'qs' ),
		'edit_item'                  => __( 'Edit Event Type', 'qs' ),
		'update_item'                => __( 'Update Event Type', 'qs' ),
		'view_item'                  => __( 'View Event Type', 'qs' ),
		'separate_items_with_commas' => __( 'Separate Event Types with commas', 'qs' ),
		'add_or_remove_items'        => __( 'Add or remove Event Types', 'qs' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'qs' ),
		'popular_items'              => __( 'Popular Event Types', 'qs' ),
		'search_items'               => __( 'Search Event Types', 'qs' ),
		'not_found'                  => __( 'Not Found', 'qs' ),
		'items_list'                 => __( 'Event Types list', 'qs' ),
		'items_list_navigation'      => __( 'Event Types list navigation', 'qs' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'                    => false,
		'show_in_rest'				 => true,
		'rest_base'					 => 'eventstype',
		'rest_controller_class'		 => 'WP_REST_Terms_Controller',				
	);
	register_taxonomy( 'events-type', array( 'sfwd-events' ), $args );

	$labels = array(
		'name'                       => _x( 'Event Sub-Category', 'Taxonomy General Name', 'qs' ),
		'singular_name'              => _x( 'Event Sub-Category', 'Taxonomy Singular Name', 'qs' ),
		'menu_name'                  => __( 'Event Sub-Category', 'qs' ),
		'all_items'                  => __( 'All Event Sub-Categories', 'qs' ),
		'parent_item'                => __( 'Parent Event Sub-Category', 'qs' ),
		'parent_item_colon'          => __( 'Parent Event Sub-Category:', 'qs' ),
		'new_item_name'              => __( 'New Event Sub-CategoryName', 'qs' ),
		'add_new_item'               => __( 'Add New Event Sub-Category', 'qs' ),
		'edit_item'                  => __( 'Edit Event Sub-Category', 'qs' ),
		'update_item'                => __( 'Update Event Sub-Category', 'qs' ),
		'view_item'                  => __( 'View Event Sub-Category', 'qs' ),
		'separate_items_with_commas' => __( 'Separate Event Sub-Categories with commas', 'qs' ),
		'add_or_remove_items'        => __( 'Add or remove Event Sub-Categories', 'qs' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'qs' ),
		'popular_items'              => __( 'Popular Event Sub-Categories', 'qs' ),
		'search_items'               => __( 'Search Event Sub-Categories', 'qs' ),
		'not_found'                  => __( 'Not Found', 'qs' ),
		'items_list'                 => __( 'Event Sub-Categories list', 'qs' ),
		'items_list_navigation'      => __( 'Event Sub-Categories list navigation', 'qs' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'                    => false,
		'show_in_rest'				 => true,
		'rest_base'					 => 'eventscategory',
		'rest_controller_class'		 => 'WP_REST_Terms_Controller',				
	);
	register_taxonomy( 'events-category', array( 'sfwd-events' ), $args );

	
	$labels = array(
		'name'                       => _x( 'Event Location', 'Taxonomy General Name', 'qs' ),
		'singular_name'              => _x( 'Event Location', 'Taxonomy Singular Name', 'qs' ),
		'menu_name'                  => __( 'Event Location', 'qs' ),
		'all_items'                  => __( 'All Event Locations', 'qs' ),
		'parent_item'                => __( 'Parent Event Location', 'qs' ),
		'parent_item_colon'          => __( 'Parent Event Location:', 'qs' ),
		'new_item_name'              => __( 'New Event Location Name', 'qs' ),
		'add_new_item'               => __( 'Add New Event Location', 'qs' ),
		'edit_item'                  => __( 'Edit Event Location', 'qs' ),
		'update_item'                => __( 'Update Event Location', 'qs' ),
		'view_item'                  => __( 'View Event Location', 'qs' ),
		'separate_items_with_commas' => __( 'Separate Event Locations with commas', 'qs' ),
		'add_or_remove_items'        => __( 'Add or remove Event Locations', 'qs' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'qs' ),
		'popular_items'              => __( 'Popular Event Locations', 'qs' ),
		'search_items'               => __( 'Search Event Locations', 'qs' ),
		'not_found'                  => __( 'Not Found', 'qs' ),
		'items_list'                 => __( 'Event Locations list', 'qs' ),
		'items_list_navigation'      => __( 'Event Locations list navigation', 'qs' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'                    => false,
		'show_in_rest'				 => true,
		'rest_base'					 => 'eventslocation',
		'rest_controller_class'		 => 'WP_REST_Terms_Controller',				
	);
	register_taxonomy( 'events-location', array( 'sfwd-events' ), $args );

	# tasks categories
	$labels = array(
		'name'                       => _x( 'Task Categories', 'Taxonomy General Name', 'qs' ),
		'singular_name'              => _x( 'Task Category', 'Taxonomy Singular Name', 'qs' ),
		'menu_name'                  => __( 'Task Category', 'qs' ),
		'all_items'                  => __( 'All Categories', 'qs' ),
		'parent_item'                => __( 'Parent Category', 'qs' ),
		'parent_item_colon'          => __( 'Parent Category:', 'qs' ),
		'new_item_name'              => __( 'New CategoryName', 'qs' ),
		'add_new_item'               => __( 'Add New Category', 'qs' ),
		'edit_item'                  => __( 'Edit Category', 'qs' ),
		'update_item'                => __( 'Update Category', 'qs' ),
		'view_item'                  => __( 'View Category', 'qs' ),
		'separate_items_with_commas' => __( 'Separate Categories with commas', 'qs' ),
		'add_or_remove_items'        => __( 'Add or remove Categories', 'qs' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'qs' ),
		'popular_items'              => __( 'Popular Categories', 'qs' ),
		'search_items'               => __( 'Search Categories', 'qs' ),
		'not_found'                  => __( 'Not Found', 'qs' ),
		'items_list'                 => __( 'Categories list', 'qs' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'qs' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'                    => false,
		'show_in_rest'				 => true,
		'rest_base'					 => 'tasktax',
		'rest_controller_class'		 => 'WP_REST_Terms_Controller',				
	);
	register_taxonomy( 'tasktax', array( 'sfwd-lessons' ), $args );

}
add_action( 'init', 'qs_course_taxonomy', 0 );

# custom post types
function qs_register_cpt(){

	# lessons
	$labels = array(
		'name'                => _x( 'Tasks', 'Post Type General Name', 'qs' ),
		'singular_name'       => _x( 'Task', 'Post Type Singular Name', 'qs' ),
		'menu_name'           => __( 'Task', 'qs' ),
		'name_admin_bar'      => __( 'Task', 'qs' ),
		'parent_item_colon'   => __( 'Parent Task:', 'qs' ),
		'all_items'           => __( 'All Tasks', 'qs' ),
		'add_new_item'        => __( 'Add New Task', 'qs' ),
		'add_new'             => __( 'Add New', 'qs' ),
		'new_item'            => __( 'New Task', 'qs' ),
		'edit_item'           => __( 'Edit Task', 'qs' ),
		'update_item'         => __( 'Update Task', 'qs' ),
		'view_item'           => __( 'View Task', 'qs' ),
		'search_items'        => __( 'Search Task', 'qs' ),
		'not_found'           => __( 'Not found', 'qs' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'qs' ),
	);
	$args = array(
		'label'               => __( 'Task', 'qs' ),
		'description'         => __( 'Individual tasks to be completed.', 'qs' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'          => array( 'tasktax' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_in_rest'       => true,
        'rest_base'          => 'lessons',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
		'capability_type'     => 'page',
		'rewrite' => array(
			'slug' 		 => 'lessons',
			'with_front' => false,
		)
	);
	register_post_type( 'sfwd-lessons', $args );

	// Courses and course taxonomies will be available only in version 1
	if (un_child_version()==1) {

		#courses
		$labels = array(
			'name'                => _x( 'Courses', 'Post Type General Name', 'qs' ),
			'singular_name'       => _x( 'Course', 'Post Type Singular Name', 'qs' ),
			'menu_name'           => __( 'Course', 'qs' ),
			'name_admin_bar'      => __( 'Course', 'qs' ),
			'parent_item_colon'   => __( 'Parent Course:', 'qs' ),
			'all_items'           => __( 'All Courses', 'qs' ),
			'add_new_item'        => __( 'Add New Course', 'qs' ),
			'add_new'             => __( 'Add New', 'qs' ),
			'new_item'            => __( 'New Course', 'qs' ),
			'edit_item'           => __( 'Edit Course', 'qs' ),
			'update_item'         => __( 'Update Course', 'qs' ),
			'view_item'           => __( 'View Course', 'qs' ),
			'search_items'        => __( 'Search Course', 'qs' ),
			'not_found'           => __( 'Not found', 'qs' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'qs' ),
		);
		$args = array(
			'label'               => __( 'Course', 'qs' ),
			'description'         => __( 'A collection of Tasks.', 'qs' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
			'taxonomies'          => array( 'coursetax', ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false,		
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'show_in_rest'       => true,
	        'rest_base'          => 'courses',
	        'rest_controller_class' => 'WP_REST_Posts_Controller',
			'capability_type'     => 'page',
			'rewrite' => array(
				'slug' 		 => 'courses',
				'with_front' => false,
			)
			
		);
		register_post_type( 'sfwd-courses', $args );

	}

	#events
	$labels = array(
		'name'                => _x( 'Events', 'Post Type General Name', 'qs' ),
		'singular_name'       => _x( 'Events', 'Post Type Singular Name', 'qs' ),
		'menu_name'           => __( 'Events', 'qs' ),
		'name_admin_bar'      => __( 'Events', 'qs' ),
		'parent_item_colon'   => __( 'Parent Events:', 'qs' ),
		'all_items'           => __( 'All Events', 'qs' ),
		'add_new_item'        => __( 'Add New Events', 'qs' ),
		'add_new'             => __( 'Add New', 'qs' ),
		'new_item'            => __( 'New Events', 'qs' ),
		'edit_item'           => __( 'Edit Events', 'qs' ),
		'update_item'         => __( 'Update Events', 'qs' ),
		'view_item'           => __( 'View Events', 'qs' ),
		'search_items'        => __( 'Search Events', 'qs' ),
		'not_found'           => __( 'Not found', 'qs' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'qs' ),
	);
	$args = array(
		'label'               => __( 'Events', 'qs' ),
		'description'         => __( 'Webinars and Live Training', 'qs' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'          => array( 'events-type', 'events-location', ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,		
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'show_in_rest'       => true,
        'rest_base'          => 'events',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
		'capability_type'     => 'page',
		'rewrite' => array(
			'slug' 		 => 'events',
			'with_front' => false,
		)
		
	);
	register_post_type( 'sfwd-events', $args );

	
}
add_action( 'init', 'qs_register_cpt', 0 );

// Register Jobs
function un_qs_job_type_post_type_init() {

	// Jobs will be available only in version 2
	if (un_child_version()==2) {

		$labels = array(
			'name'                  => _x( 'Job Types', 'Post Type General Name', 'un' ),
			'singular_name'         => _x( 'Job Type', 'Post Type Singular Name', 'un' ),
			'menu_name'             => __( 'Job Types', 'un' ),
			'name_admin_bar'        => __( 'Job Type', 'un' ),
			'archives'              => __( 'Job Type Archives', 'un' ),
			'attributes'            => __( 'Job Type Attributes', 'un' ),
			'parent_item_colon'     => __( 'Parent Job Type:', 'un' ),
			'all_items'             => __( 'All Job Types', 'un' ),
			'add_new_item'          => __( 'Add New Job Type', 'un' ),
			'add_new'               => __( 'Add New', 'un' ),
			'new_item'              => __( 'New Job Type', 'un' ),
			'edit_item'             => __( 'Edit Job Type', 'un' ),
			'update_item'           => __( 'Update Job Type', 'un' ),
			'view_item'             => __( 'View Job Type', 'un' ),
			'view_items'            => __( 'View Job Types', 'un' ),
			'search_items'          => __( 'Search Job Type', 'un' ),
			'not_found'             => __( 'Not found', 'un' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'un' ),
			'featured_image'        => __( 'Featured Image', 'un' ),
			'set_featured_image'    => __( 'Set featured image', 'un' ),
			'remove_featured_image' => __( 'Remove featured image', 'un' ),
			'use_featured_image'    => __( 'Use as featured image', 'un' ),
			'insert_into_item'      => __( 'Insert into Job Type', 'un' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Job Type', 'un' ),
			'items_list'            => __( 'Job Types list', 'un' ),
			'items_list_navigation' => __( 'Job Types list navigation', 'un' ),
			'filter_items_list'     => __( 'Filter Job Types list', 'un' ),
		);
		$args = array(
			'label'                 => __( 'Job Type', 'un' ),
			'description'           => __( 'Job Type', 'un' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'thumbnail', 'revisions', 'custom-fields', ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-admin-tools',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,		
			'exclude_from_search'   => true,
			'publicly_queryable'    => false,
			'query_var'             => 'job_type',
			'rewrite'               => false,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		);
		register_post_type( 'job_type', $args );
	
	}

}

add_action( 'init', 'un_qs_job_type_post_type_init', 0 );



