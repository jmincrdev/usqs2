<?php
function un_get_transient_lesson_banner($number) {
		
	$post_meta_name = 'un_lesson_banner';
	$un_lesson_banner = 'un-theme-lesson-codeblock';
	
	$custom_block = get_post_meta( $post_id, $post_meta_name, true );
	
	if (is_numeric($post_id) and $custom_block) {
		return 	apply_filters("the_content", get_post($custom_block)->post_content);
	}
	else
	{
		if ($option = Option::get("un-global-setting-section", $un_lesson_banner)) {				
			if (class_exists('WPBMap')) WPBMap::addAllMappedShortcodes();
			$block = get_post($option);
			return 	$block ? apply_filters("the_content", $block->post_content) : "NADA";
		}
	}
		
}

function un_get_related_lessons($post_id = null) {

	if(!$post_id){
		$post_id = get_the_ID();
	}
	
	
	
	
	$results_id = get_crp_posts_id(['postid' => $post_id, 'limit' => 3, "post_types" => ['sfwd-lessons'] ]);
	
	$results		= [];
	foreach ($results_id as $article) {
		$results[] = get_post($article->ID);
	}
	
	return $results;

}

add_filter( 'crp_posts_post_types', 'un_crp_only_sent_postype' );

function un_crp_only_sent_postype($post_types, $post_id){
	
	if(get_post_type() == "sfwd-lessons" ){
		unset($post_types['post']);
		unset($post_types['page']);
	}
	return $post_types;
	
}


function un_get_lesson_related_posts(){
	if(count(get_related_posts()) > 0){
		return [ "type" => "custom", "posts" => get_related_posts() ];
	}else{
		return [ "type" => "auto", "posts" => get_related_posts_plugin() ];
	}
}
		