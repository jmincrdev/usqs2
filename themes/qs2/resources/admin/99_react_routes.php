<?php

# add main endpoints of our system
add_action( 'rest_api_init', function () {

	register_rest_route( 'un/v1', '/events/', array(
		'methods'		=> 'GET',
		'callback'		=> 'un_get_events',
	) );

});

 
# Get events
function un_get_events( $request ) {

	$transient = "rest_api_qs2_events";

	# do we have a cache? return it
	if ($cached_verion = un_get_local_cache($transient)) {
		return $cached_verion;
	}

	// get all events or by cat
	if ($cat = $_GET["cat"] and is_numeric($cat)) {
		$extra = [
			'tax_query' => [
                [
	                'taxonomy' => 'events-category',
	                'field' => 'term_id',
	                'terms' => $cat
	            ]
	        ]
	    ];
	} else {
		$extra = [];
	}

	$args =  array_merge([ 'post_type' => 'sfwd-events', 'posts_per_page'=>-1], $extra);

	$posts_query = new WP_Query($args);

	foreach($posts_query->posts as $i=>$post) {

		// get all ACF
		$posts_query->posts[$i]->acf = get_fields($post->ID);
			
		// get permalink
		$posts_query->posts[$i]->permalink = get_permalink($post->ID);

		$higher_time=0;
		// cycle all events dates and take the higher
		foreach( $posts_query->posts[$i]->acf["event_repeater"] as $event ) {

			if ($event["date"] and $event["end_time"]) {

				$event_date_time = strtotime($event["date"] . " " . $event["end_time"]);

				if (!$higher_time or $higher_time<$event_date_time) {
					$higher_time = $event_date_time;
				}

			}

		}
		
		if ( $higher_time + 24*60*60 >= time() ) {
			$result[$higher_time."_".$post->ID]=$post;
		}
		
	}

	ksort($result);

	$out = array_values($result);

	# save local cache
	un_set_local_cache($transient, $out);

	return new WP_REST_Response( $out, 200 );

}


# add main endpoints of our system
add_action( 'rest_api_init', function () {

	register_rest_route( 'un/v1', '/get_jobs/', array(
		'methods'		=> 'GET',
		'callback'		=> 'un_get_jobs',
	) );

});


function cmp($a, $b)
{
    return strcmp($a["order"], $b["order"]);
}

# Get Jobs
function un_get_jobs( $request ) {

	$args =  array_merge([ 
		'post_type' => 'job_type', 
		'posts_per_page' => -1
	]);

	$posts_query = new WP_Query($args);

	$result = [];

	foreach($posts_query->posts as $i=>$post) {

		// get all ACF
		$acf = get_fields($post->ID);
		
		$result[] = [
			'job_id' 	=> $post->ID,
			'name' 		=> $post->post_title,
			'high_icon'	=> $acf["high_icon"],
			'gray_icon'	=> $acf["gray_icon"],
			'order'		=> $acf["order"],
		];

	}

	usort($result, "cmp");

	return new WP_REST_Response( $result, 200 );

}

# add main endpoints of our system
add_action( 'rest_api_init', function () {

	register_rest_route( 'un/v1', '/get_lessons/', array(
		'methods'		=> 'GET',
		'callback'		=> 'un_get_lessons',
	) );

});


function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}


# Get Jobs
function un_get_lessons( $request ) {

	// get parameters
	$jobid 		= sanitize_text_field($_GET["job"]);
	$content 	= sanitize_text_field($_GET["content"]);
	$type 		= strtoupper(sanitize_text_field($_GET["type"]));
	$page 		= sanitize_text_field($_GET["page"]);
	$n	 		= sanitize_text_field($_GET["n"]);

	// set defaults
	if (!$content)	$content='video';
	if (!$type)		$type='SMB';
	if (!$page)		$page=1;
	if (!$n) 		$n=6;

	$args =  array_merge([ 
		'post_type' => 'sfwd-lessons', 
		'posts_per_page' => -1,
	]);

	// Filter data by content: only if it is SMB
	// Filter data by type

	if ($type=="SMB") {

		$cond = [
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'key'	 	=> 'lesson_type',
					'value'	  	=> 'SMB',
					'compare' 	=> '=',
				),
				array(
					'key'	  	=> 'lesson_content',
					'value'	  	=> $content,
					'compare' 	=> '=',
				),
			)
		];

	} else {

		$cond = [
			'meta_key'		=> 'lesson_content',
			'meta_value'	=> $content,
		];

	}

	$args = array_merge($args, $cond);

	$posts_query = new WP_Query($args);

	// get all lessons of a job

	$unfiltered_results = [];

	foreach($posts_query->posts as $i=>$post) {

		// get all ACF
		$acf = get_fields($post->ID);

		foreach ($acf['bound_jobs'] as $job) {

			if ($job->ID == $jobid) {

				// pagination logic, skip the first x results
				if ($skip>0) {
					$skip--;
					break;
				}

				// get thumbnail, prioritize featured image in post
				$the_image = get_the_post_thumbnail_url($post->ID);

				// if there is no featured image get from YouTube
				if (!$the_image) {

					$youtube_image = qs_youtube_thumbnail($post->ID);

					if ($youtube_image['hqDefault']) {
						$the_image = $youtube_image['hqDefault'];
					} elseif ($youtube_image['default']) {
						$the_image = $youtube_image['default'];
					}

				}

				$unfiltered_results[] = [
					"id" 		=> $post->ID,
					"title" 	=> $post->post_title,
					"url" 		=> get_permalink($post->ID),
					"image"		=> $the_image,
					"time"		=> qs_youtube_duration($post->ID),
					"type"		=> $acf['lesson_type'],
					"content"	=> $acf['lesson_content'],
					"priority"	=> $acf['priority'],
				];

				break;

			}

		}

	}

	/*
	- If the Job is an accountant type:
		- Show the accountant tasks (lessons) first
		- Then show the next lessons (not accountant) following this rules:
			- Show first the lessons that have this Job as Job 1, then Job 2, Job 3...
			- Order this videos according to priority
	- If the Job is an Small Business type:
		- Show first the lessons that have this Job as Job 1, then Job 2, Job 3...
		- Order this videos according to priority
	*/

	$unfiltered_results = array_orderby($unfiltered_results, 'type', SORT_ASC, 'priority', SORT_ASC);	

	// do the pagination

	$count = 0;

	$skip = $skip_orig = ($page - 1) * $n;

	$result = [];

	foreach($unfiltered_results as $post) {

		// pagination logic, skip the first x results
		if ($skip>0) {

			$skip--;

		// save results if all initial results were skipped
		} else {

			$result[] = $post;

			$count++;

			// quit if we got n results
			if ($count>=$n) {
				break;
			}

		}

	}

	if ($count + $skip_orig >= sizeof($unfiltered_results)) {
		$last=true;
	} else {
		$last=false;
	}

	return new WP_REST_Response( ['last' => $last, 'lessons' => $result], 200 );

}

add_action( 'rest_api_init', function () {

	register_rest_route( 'un/v1', '/tag-lessons/', array(
		'methods'		=> 'GET',
		'callback'		=> 'un_get_tag_lessons',
	) );

});

function un_get_tag_lessons(){
	
	$tag		= strtoupper(sanitize_text_field($_GET["tag"]));
	$page 		= sanitize_text_field($_GET["page"]);
	$n	 		= sanitize_text_field($_GET["n"]);

	// set defaults
	if (!$tag)		$tag='';
	if (!$page)		$page=1;
	if (!$n) 		$n=6;
		
	$args =  array_merge([ 
		'post_type' => 'sfwd-lessons', 
		'posts_per_page' => 6,
		'tag' => $tag,
		'paged' => $page,
		'orderby' => 'post_date',
		'order' => 'DESC'
	]);

	$posts_query = new WP_Query($args);
	
	$result = $posts_query;
	
	#to improve security ;)
	$result->request = null;
	$result->tax_query = null;
	
	foreach ($result->posts as $post) {
		$post->permalink = get_permalink($post->ID);
		$post->thumbnail = get_the_post_thumbnail_url($post->ID);
		$post->time = qs_youtube_duration($post->ID);
	}
	
	return new WP_REST_Response( $result, 200 );
}

add_action( 'rest_api_init', function () {

	register_rest_route( 'un/v1', '/header-menu/', array(
		'methods'		=> 'GET',
		'callback'		=> 'un_header_menu',
	) );

});

function un_header_menu(){
	
	array_push(
		$settings['un-global-setting-section'],
		Field::text(
			'un-small-business',
			[
				'title' => 'Small Business Tag',
				'default' => 'Small Business'
			]
		)
	);

	array_push(
		$settings['un-global-setting-section'],
		Field::text(
			'un-accountants',
			[
				'title' => 'Accountants Tag',
				'default' => 'Accountants'
			]
		)
	);
	
	$acc = wp_get_nav_menu_items(Option::get("un-global-setting-section", "un-accountants"));
	if (!$acc) {
		$acc = "Accountants";
	}
	$smb = wp_get_nav_menu_items(Option::get("un-global-setting-section", "un-small-business"));
	if (!$smb) {
		$smb = "Small business";
	}

	return new WP_REST_Response( 
		[
			['title' => $smb, "type" => "SMB"],
			['title' => $acc, "type" => "ACC"],
		]
	, 200 );

}