<?php # our makeshift translation file

#API
	define("QS_YOUTUBE_API_KEY", "AIzaSyAXYoo0k2HqORCMhznCmnAnOXFKgGU90Ac");

#home
	define("QS_SEARCH_TUTORIALS", "Search Tutorials");
	define("QS_LEARN_QB", "Learn QuickBooks Today");
	define("QS_SEE_ALL_TUTORIALS", "See all QuickBooks tutorials");
	define("QS_LESSON_INDEX_URL", "http://quickbooks.intuit.com/tutorials/all-quickbooks-tutorials/");

#cta
	define("QS_FOOTER_FREE_TRIAL_CTA", "Try QuickBooks for free,<br>no credit card needed.");
	define("QS_FOOTER_FREE_TRIAL_BUTTON_TEXT", "Start Free Trial");
	define("QS_FOOTER_FREE_TRIAL_BUTTON_LINK", "http://quickbooks.intuit.com/tutorials/course-index/#");
	
#footer
	define("QS_FOOTER_LEGAL", "Intuit Inc. All rights reserved. Intuit and QuickBooks are registered trademarks of Intuit Inc. Terms and conditions, features, support, pricing, and service options subject to change without notice.");
	
#course
	define("QS_COURSE_INDEX_URL", "http://quickbooks.intuit.com/tutorials/course-index/");
	define("QS_COURSE_ALL", "All courses");
	define("QS_COURSE_START", "Get Started");
	define("QS_COURSE_CLICK", "click any tutorial");

#lesson
/* DEPRECATED please refer now to:
 * Theme Options -> Quickstart Localization -> Lesson
	define("QS_LESSON_SBS", "Read step-by-step instructions");
	define("QS_LESSON_CURRENT", "Current Task");
	define("QS_DONT_HAVE", "Don't have QuickBooks?");
	define("QS_START_TRIAL", "Start My Free Trial");
	define("QS_START_TRIAL_URL", "http://quickbooks.intuit.com/signup/");
	define("QS_STEP_BY_STEP", "Open step-by-step instructions");
*/

#search
	define("QS_SEARCH", "Search");
	define("QS_SEARCH_RESULTS", "Search Results");
	define("QS_SEARCH_YOU_QUERIED", "You searched for");
	define("QS_SEARCH_LESSON_RESULT", "Task results");
	define("QS_SEARCH_LESSON_NO_RESULT", "No Tasks found");	
	define("QS_SEARCH_COURSE_RESULT", "Course results");