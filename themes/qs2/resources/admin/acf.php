<?php ## this file loads in all of our ACF fields via config, rather than manually building them.

if(function_exists("register_field_group")){

	if (un_child_version()==1) {

		## COURSES
		register_field_group(array (
			'id' => 'acf_courses',
			'title' => 'Courses',
			'fields' => array (
				array (
					'key' => 'field_563a6ee7b83cd',
					'label' => 'Associated Lessons',
					'name' => 'bound_lessons',
					'type' => 'relationship',
					'instructions' => 'Select the Lessons that belong to this Course.',
					'return_format' => 'object',
					'post_type' => array (
						0 => 'sfwd-lessons',
					),
					'taxonomy' => array (
						0 => 'all',
					),
					'filters' => array (
						0 => 'search',
					),
					'result_elements' => array (
						0 => 'featured_image',
						1 => 'post_type',
						2 => 'post_title',
					),
					'max' => '',
				),
				array (
					'key' => 'field_55f3a6ff003ce',
					'label' => 'Excerpt',
					'name' => 'excerpt',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_55f3bfd1a1b79',
					'label' => 'Course Text',
					'name' => 'course_text',
					'type' => 'wysiwyg',
					'default_value' => '',
					'toolbar' => 'full',
					'media_upload' => 'yes',
				),
				array (
					'key' => 'field_55f62f3ea2d6f',
					'label' => 'Transparent Image',
					'name' => 'transparent_image',
					'type' => 'image',
					'instructions' => 'This image is displayed on the course page. It needs to be transparent background or uses #393a3d as the background color. 90 x 95px',
					'save_format' => 'url',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sfwd-courses',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'default',
				'hide_on_screen' => array (
					0 => 'the_content',
					1 => 'discussion',
					2 => 'comments',
					3 => 'slug',
					4 => 'format',
					5 => 'categories',
					6 => 'tags',
					7 => 'send-trackbacks',
				),
			),
			'menu_order' => 0,
		));

	}


	$lessons_acf_fields = array (
		array (
			'key' => 'field_55f37b40268fe',
			'label' => 'Video',
			'name' => 'video',
			'type' => 'text',
			'instructions' => 'Enter YouTube video ID',
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
		array (
			'key' => 'field_55f37b5d268ff',
			'label' => 'Intro',
			'name' => 'intro',
			'type' => 'textarea',
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'formatting' => 'br',
		),
		array (
			'key' => 'field_5645193801d4e',
			'label' => 'Hide Content?',
			'name' => 'hide_content',
			'type' => 'true_false',
			'instructions' => 'Hide the content area starting "Read step-by-step instructions" and below.	',
			'message' => 'Check for lessons with a video but no main content area text/steps',
			'default_value' => 0,
		),
		array (
			'key' => 'field_565caa06f14a7',
			'label' => 'Try/Buy Buttons',
			'name' => 'cta_button_group',
			'type' => 'repeater',
			'sub_fields' => array (

				array (
					'key' => 'field_jm565caa67f14a8',
					'label' => 'Button title',
					'name' => 'button_title',
					'type' => 'text',
					'required' => 1,
					'column_width' => '',
					'default_value' => 'Try it in QuickBooks Now',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_565caa67f14a8',
					'label' => 'Button link',
					'name' => 'button_link',
					'type' => 'text',
					'required' => 1,
					'column_width' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_sc565caa67f14a8',
					'label' => 'SiteCatalyst Value',
					'name' => 'site_cat_val',
					'type' => 'text',
					'required' => 0,
					'column_width' => '',
					'default_value' => 'quickstart-guide-tryactivity-',
					'placeholder' => 'quickstart-guide-tryactivity-',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_565caac7f14a9',
					'label' => 'Button color',
					'name' => 'button_color',
					'type' => 'color_picker',
					'column_width' => '',
					'default_value' => '#2c9f1c',
				),
				array (
					'key' => 'field_565cab37f14ab',
					'label' => 'Button Icon',
					'name' => 'button_icon',
					'type' => 'image',
					'column_width' => '',
					'save_format' => 'url',
					'preview_size' => 'full',
					'library' => 'all',
				),
			),
			'row_min' => '',
			'row_limit' => '',
			'layout' => 'row',
			'button_label' => 'Add another button',
		),
	);

	// use extra ACF fields if we are in theme v2
	if (un_child_version()==2) {

		// Related Jobs
		$lessons_acf_fields[] = [
			'key' => 'field_563a6ee7b8148',
			'label' => 'Associated Jobs',
			'name' => 'bound_jobs',
			'type' => 'relationship',
			'instructions' => 'Select the Jobs that belong to this Lessons.',
			'return_format' => 'object',
			'post_type' => array (
				0 => 'job_type',
			),
			'taxonomy' => array (
				0 => 'all',
			),
			'filters' => array (
				0 => 'search',
			),
			'result_elements' => array (
				0 => 'featured_image',
				1 => 'post_type',
				2 => 'post_title',
			),
			'max' => '',
		];

		// Priority
		$lessons_acf_fields[] = [
			'key' => 'field_aab25ee7b8987',
			'label' => 'Priority',
			'name' => 'priority',
			'type' => 'select',
			'instructions' => 'Set priority',
			'return_format' => 'object',
			'choices' => array(
				'1'	=> 'High',
				'2'	=> 'Low',
			),
		];

		// Type
		$lessons_acf_fields[] = [
			'key' => 'field_77625ee7b8aa3',
			'label' => 'Type',
			'name' => 'lesson_type',
			'type' => 'select',
			'instructions' => 'Select type',
			'return_format' => 'object',
			'choices' => array(
				'SMB'	=> 'Small Business',
				'ACC'	=> 'Accountant',
			),
		];

		// Content
		$lessons_acf_fields[] = [
			'key' => 'field_88915ee7b8ba2',
			'label' => 'Content',
			'name' => 'lesson_content',
			'type' => 'select',
			'instructions' => 'Select content',
			'return_format' => 'object',
			'choices' => array(
				'video'		=> 'Video',
				'webinar'	=> 'Webinar',
			),
		];


		## register here ACF for Jobs
		register_field_group(array (
			'id' => 'acf_jobs',
			'title' => 'Jobs',
			'fields' => [
				[
					'key' => 'field_6957a6ff0cab1',
					'label' => 'Order in the menu',
					'name' => 'order',
					'type' => 'text',
					'instructions' => 'Please set here the order of this icon in the menu',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				],
				[
					'key' => 'field_2327ba53d6187',
					'label' => 'Highlighted icon',
					'name' => 'high_icon',
					'type' => 'image',
					'instructions' => 'Please set green (highlighted icon)',
					'save_format' => 'url',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				],
				[
					'key' => 'field_15763232d6fea',
					'label' => 'Gray icon',
					'name' => 'gray_icon',
					'type' => 'image',
					'instructions' => 'Please set gray (unselected icon)',
					'save_format' => 'url',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				]
			],
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'job_type',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
		
		
		$lessons_acf_fields[] = array (
				'key' => 'field_5581ba4174607',
				'label' => 'Custom Related Posts',
				'name' => 'custom_related_posts',
				'type' => 'relationship',
				'instructions' => 'Manually set up to 3 related posts for this article.',
				'return_format' => 'id',
				'post_type' => array (
					0 => 'sfwd-lessons'
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'filters' => array (
					0 => 'search',
					1 => 'post_type',
				),
				'result_elements' => array (
					0 => 'featured_image',
					1 => 'post_type',
					2 => 'post_title',
				),
				'max' => 3,
			);
		
		
		$lessons_acf_fields[] = array (
				'key' => 'field_58e4c7a68ert91',
				'label' => 'Banner',
				'name' => 'un_lesson_banner',
				'type' => 'post_object',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'code-block',
				),
				'taxonomy' => array (
				),
				'allow_null' => 0,
				'multiple' => 0,
				'return_format' => 'id',
				'ui' => 1,
		);
		
		
		

	}

	## LESSONS
	register_field_group(array (
		'id' => 'acf_lesson',
		'title' => 'Lesson',
		'fields' => $lessons_acf_fields,
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-lessons',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

}
