<?php

// Child Theme Hooks

// load themosis NS
use \Themosis\Facades;
use \Themosis\Page;

// add banner section for QS
function un_global_setting_section_settings($settings) {

	$CodeBlocks = [];

	foreach (CodeBlockModel::all() as $key => $value) {
	    $CodeBlocks[$value->ID] = $value->post_title;
	}
	
	array_push(
		$settings['un-global-setting-section'],
		Field::text(
			'un-theme-qs-banner',
			['title' => 'QS Banner']
		)
	);
	
	array_push(
		$settings['un-global-setting-section'],
		Field::select(
			'un-theme-child-version',
			[['version01' => 'Version 1', 'version02' => 'Version 2']],
			['title' => 'Theme Version']
		)
	);
	
	array_push(
		$settings['un-global-setting-section'],
		Field::select(
			'un-theme-lesson-codeblock',
			[ $CodeBlocks ],
			['title' => 'Lesson Default Banner']
		)
	);
	
	array_push(
		$settings['un-global-setting-section'],
		Field::text(
			'un-site-name',
			['title' => 'Site name']
		)
	);

	$menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
	
	$arr_menus=[];
	foreach ($menus as $m) {
		$arr_menus[$m->term_id] = $m->name;
	}

	array_push(
		$settings['un-global-setting-section'],
		Field::text(
			'un-small-business',
			[
				'title' => 'Small Business Tag',
				'default' => 'Small Business'
			]
		)
	);

	array_push(
		$settings['un-global-setting-section'],
		Field::text(
			'un-accountants',
			[
				'title' => 'Accountants Tag',
				'default' => 'Accountants'
			]
		)
	);


/*
	array_push(
		$settings['un-global-setting-section'],
		Field::select(
			'un-header-menu',
			[$arr_menus],
			['title' => 'Header menu']
		)
	);
*/
	return $settings;
}

// return banner section code block id
function un_child_theme_footer($post_id=null) {
	
	if (!$post_id) {
		$post_id = get_the_ID();
	}

	// Call To Action Banner (CTA)
	if ($post_id) {
		$cta_banner = get_post_meta( $post_id, 'un_custom_cta', true );		
	}
	if ($cta_banner==-1) { // none
		return 0;
	} elseif ($cta_banner>0) { //  a custom CTA Banner for this page
		return $cta_banner;
	} else { // default CTA banner
		return Option::get("un-global-setting-section", "un-theme-qs-banner");
	}

}

// end hooks

// Localization

function un_translation_page($page)
{
	$qs_localization = \Themosis\Facades\Page::make('qs-localization', 'Quickstart Localization', 'un-global-settings')->set([
		'capability'	=> 'manage_options',
		'icon'			=> 'dashicons-admin-site',
		'position'      => 1000,
		'tabs'			=> true,
		'menu'			=> 'Quickstart Localization',
	]);

	# the subsections we will use
	$sections[] = \Themosis\Facades\Section::make('qs-localization-tutorial', 'Tutorial');
	$sections[] = \Themosis\Facades\Section::make('qs-localization-lesson', 'Lesson');
	$sections[] = \Themosis\Facades\Section::make('qs-localization-search', 'Search');

	$settings['qs-localization-tutorial'] = [
		\Themosis\Facades\Field::text('QS_TUTORIAL_ALL', ['title' => 'All courses']),
		\Themosis\Facades\Field::text('QS_TUTORIAL_CLICK', ['title' => 'click any tutorial']),
		\Themosis\Facades\Field::text('QS_TUTORIAL_INDEX_URL', ['title' => 'Url index tutorial']),
	];
	
	$settings['qs-localization-lesson'] = [
		\Themosis\Facades\Field::text('QS_LESSON_SBS', ['title' => 'Read step-by-step instructions']),
		\Themosis\Facades\Field::text('QS_LESSON_CURRENT', ['title' => 'Current Task']),
		\Themosis\Facades\Field::text('QS_DONT_HAVE', ['title' => 'Don\'t have QuickBooks?']),
		\Themosis\Facades\Field::text('QS_START_TRIAL', ['title' => 'Start My Free Trial']),
		\Themosis\Facades\Field::text('QS_START_TRIAL_URL', ['title' => 'Start Trial URL']),
		\Themosis\Facades\Field::text('QS_STEP_BY_STEP', ['title' => 'Open step-by-step instructions']),
	];
	
	$settings['qs-localization-search'] = [
		\Themosis\Facades\Field::text('QS_SEARCH_RESULTS', ['title' => 'Search Results']),
		\Themosis\Facades\Field::text('QS_SEARCH_YOU_QUERIED', ['title' => 'You searched for']),
		\Themosis\Facades\Field::text('QS_SEARCH_TUTORIAL_RESULT', ['title' => 'Tutorial results']),
		\Themosis\Facades\Field::text('QS_SEARCH_COURSE_RESULT', ['title' => 'Course results']),
		\Themosis\Facades\Field::text('QS_SEARCH_QUICKBOOKS_UNIVERSITY', ['title' => 'Search Quickbooks University']),
	];	

	$qs_localization->addSections($sections);
	$qs_localization->addSettings($settings);
}

function qs_core_scripts_styles() {
    wp_enqueue_style('qs-style', get_stylesheet_directory_uri().'/resources/assets/css/style.css', array('standard-4e79f0aed445ab6c23e5b325ce4ee5ec-style','un-bootstrap-style','un-components-style','un-app-style'));
	wp_enqueue_style('qs-build', get_stylesheet_directory_uri().'/resources/assets/css/build.css', array('standard-4e79f0aed445ab6c23e5b325ce4ee5ec-style','un-bootstrap-style','un-components-style','un-app-style'));
}

add_action('wp_enqueue_scripts','qs_core_scripts_styles');

wp_enqueue_style('ls-google-fonts-css', 'http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900|Open+Sans:300|Indie+Flower:regular|Oswald:300,regular,700&#038;subset=latin,latin-ext');





/*****
 * CTA Banner
 */

// add meta data

add_action( 'init', 'un_custom_cta_banner' );
function un_custom_cta_banner() {
  register_meta( 'page', 'un_page_custom_cta_banner', 'sanitize_text_field', 'un_custom_cta_banner_callback' ); 
}

function un_custom_cta_banner_callback( $allowed, $meta_key, $post_id, $user_id, $cap, $caps ) {
  if( 'page' == get_post_type( $post_id ) && current_user_can( 'edit_post', $post_id ) ) {
      return true;
  } else  {
      return false;
  }
}

// add meta box
add_action( 'admin_menu', 'un_custom_cta_banner_meta_box' );
function un_custom_cta_banner_meta_box() {

	add_meta_box(
                 'un-cta-banner-meta-box',
                  'Call To Action',
                  'un_cta_banner_callback',
                  'page',
                  'normal',
                  'high',
                   array( 'key' => 'value' )
    );
    
}

function un_cta_banner_callback_options( $un_custom ) {

	$args = array(
			'post_type' => 'code-block',
			'orderby'	=> 'title',
			'order'		=> ASC
		);
	$loop = new WP_Query( $args );

	echo "<option value=\"-1\"";
	if ($un_custom==-1) echo " selected"; 
	echo ">None</option>";
	
	echo "<option value=\"0\"";
	if (!$un_custom) echo " selected";
	echo ">Default</option>";
	
	foreach($loop->posts as $code_block) {
		echo "<option value=\"".$code_block->ID."\"";
		if ($un_custom==$code_block->ID) echo " selected";
		echo ">".$code_block->post_title."</option>";
	}
	
}

function un_cta_banner_callback( $post, $args = array() ) {

	// Generate nonce http://codex.wordpress.org/Function_Reference/wp_nonce_field
	wp_nonce_field( 'un_cta_banner', 'un_cta_banner_noncename' );

	$un_custom_cta =  get_post_meta( $post->ID, 'un_custom_cta', true );

	?>
	<p>
		<label for="un_custom_foot">Select CTA Banner</label>
		<select name="un_custom_cta" id="un_custom_cta">
		<?php un_cta_banner_callback_options( $un_custom_cta ); ?>
		</select>
	</p>
	<?php

}

// save meta data
add_action( 'save_post', 'un_cta_banner_custom_fields', 10, 2 );
function un_cta_banner_custom_fields( $post_id, $post ){
    
    // Security check
    if ( ! isset( $_POST['un_cta_banner_noncename'] ) || ! wp_verify_nonce( $_POST['un_cta_banner_noncename'], 'un_cta_banner' ) ) {
        return;
    }
            
    // update cta
    if( isset( $_POST['un_custom_cta'] ) && $_POST['un_custom_cta'] != "" ) {
        update_post_meta( $post_id, 'un_custom_cta', $_POST['un_custom_cta'] );
    } else {
        // delete meta field if previously exists
        delete_post_meta( $post_id, 'un_custom_cta' );
    }
    
}

/****** Custom CTA BANNER END */


/* hook to add extra data to search template and search rest api*/

function un_search_extra_data() {
	return [
		'search_title'			=> get_localization('qs-localization-search', 'QS_SEARCH_RESULTS'),
		'search_text'			=> get_localization('qs-localization-search', 'QS_SEARCH_YOU_QUERIED'),
		'search_course_title'	=> get_localization('qs-localization-search', 'QS_SEARCH_COURSE_RESULT'),
		'search_tutorial_title'	=> get_localization('qs-localization-search', 'QS_SEARCH_TUTORIAL_RESULT')
	];
}

// routes endpoint hook
function un_routes_extra_data_api($out) {
	
	// add search extras
	$out["search"]["localization"]["search_title"] 			= get_localization('qs-localization-search', 'QS_SEARCH_RESULTS');
	$out["search"]["localization"]["search_text"] 			= get_localization('qs-localization-search', 'QS_SEARCH_YOU_QUERIED');
	$out["search"]["localization"]["search_course_title"] 	= get_localization('qs-localization-search', 'QS_SEARCH_COURSE_RESULT');
	$out["search"]["localization"]["search_tutorial_title"] = get_localization('qs-localization-search', 'QS_SEARCH_TUTORIAL_RESULT');
	
	// add lessons -> courses redirectors
	$args = ['post_type' => 'sfwd-courses', 'posts_per_page' => -1];
	$loop = new WP_Query( $args );

	foreach($loop->posts as $course) {
//		echo "<h1>Course: ".$course->post_name." ID: ".$course->ID." first lesson: ".get_first_lesson($course->ID, false)."</h1>".PHP_EOL;
		$the_first_lesson = get_first_lesson($course->ID, false);
		$out['redirection']['/' . $course->post_name . '/'] = '/' . $the_first_lesson . '/';
		$out['redirection']['/courses/' . $course->post_name . '/'] = '/lessons/' . $the_first_lesson . '/';
	}

	return $out;
}


/* hook for CPT used in search */

function un_custom_child_post_types() {
	return ['sfwd-lessons', 'sfwd-courses'];
}

/* hook for post extra data */
function un_child_post_extra_data($post) {
	$post['time'] 		= qs_youtube_duration($post["ID"]);	
	$post["num_lesson"] = count(get_field("bound_lessons",$post["ID"]));
	return $post;
}

/* shortcode for lessons */
function sidebar_lesson_render( $atts, $content = "" ) {
	$index_url 		= get_localization('qs-localization-tutorial', 'QS_TUTORIAL_INDEX_URL');
	$index_text 	= get_localization('qs-localization-tutorial', 'QS_TUTORIAL_ALL');
	$click_tutorial = get_localization('qs-localization-tutorial', 'QS_TUTORIAL_CLICK');
	return '<div data-qb-url="'.$index_url.'" data-qb-urltext="'.$index_text.'" data-qb-click="'.$click_tutorial.'" data-qb-courses="'.$atts['courses'].'" data-qb-sidebar-lesson></div>';
}
add_shortcode( 'sidebar-lesson', 'sidebar_lesson_render' );

function sidebar_header_render( $atts, $content = "" ) {
	$index_url 		= get_localization('qs-localization-tutorial', 'QS_TUTORIAL_INDEX_URL');
	$index_text 	= get_localization('qs-localization-tutorial', 'QS_TUTORIAL_ALL');
	$click_tutorial = get_localization('qs-localization-tutorial', 'QS_TUTORIAL_CLICK');	
	return '<div data-qb-url="'.$index_url.'" data-qb-urltext="'.$index_text.'" data-qb-click="'.$click_tutorial.'" data-qb-courses="'.$atts['courses'].'" data-qb-header-lesson></div>';
}
add_shortcode( 'header-lesson', 'sidebar_header_render' );




# add yoast to lessons
add_action( 'rest_api_init', 'qs2_lesson_un_rest_yoast_title' );
function qs2_lesson_un_rest_yoast_title() {

	$cpts = array('sfwd-lessons','sfwd-courses');

	foreach($cpts as $cpt){
		register_rest_field( $cpt,
			'metatags',
			array(
				'get_callback'    => 'rest_slug_get_field',
				'update_callback' => null,
				'schema'          => null,
			)
		);
	}
}

// Filter
add_filter( 'acf/rest_api/sfwd-courses/get_fields', function( $data ) {
	if ( method_exists( $data, 'get_data' ) ) {
		$data = $data->get_data();
	}else{
		$data = (array) $data;
	}

	if ( isset( $data['acf'] ) && isset ( $data['acf']['bound_lessons'] ) ) {

		$lessons = $data['acf']['bound_lessons'];
		foreach ($lessons as $lesson) {
			$lesson->guid = get_the_permalink($lesson->ID);
		}

		$data['lesson_num'] = count($data['acf']['bound_lessons']);

	}
	return $data;
});

function un_child_version() {
	$unoptions = get_option('un-global-setting-section');
	if (!$unoptions['un-theme-child-version'] or $unoptions['un-theme-child-version']=='version01') {
		return 1;
	} else {
		return 2;
	}	
}


# child functions hooks
function un_child_search_template() {

	$theme_child_version = Option::get("un-global-setting-section", "un-theme-child-version");
	switch($theme_child_version){
		case 'version02':
			return "un-search-" . $theme_child_version;
			break;
		default:
			return 'un-search';
	}

}


function un_child_search_extra_header() {
	return View::make('un-qs-header');
}

/* hook to add extra data to input search rest api*/
function un_search_extra_input() {
	return ['posts_per_page'	=> 6,
			'paged'				=> sbc_get_page()];
}

// add modal image on click
wp_enqueue_script('modal-js',get_stylesheet_directory_uri().'/resources/assets/js/modal.js', array(), false, true);
