<?php  if (!function_exists('lcr58dbd9e30e392encq')) { function lcr58dbd9e30e392encq($cx, $var) {
  if ($var instanceof LS) {
   return (string)$var;
  }

  return str_replace(array('=', '`', '&#039;'), array('&#x3D;', '&#x60;', '&#x27;'), htmlspecialchars(lcr58dbd9e30e392raw($cx, $var), ENT_QUOTES, 'UTF-8'));
 }}

 if (!function_exists('lcr58dbd9e30e392sec')) { function lcr58dbd9e30e392sec($cx, $v, $bp, $in, $each, $cb, $else = null) {
  $push = ($in !== $v) || $each;

  $isAry = is_array($v) || ($v instanceof \ArrayObject);
  $isTrav = $v instanceof \Traversable;
  $loop = $each;
  $keys = null;
  $last = null;
  $isObj = false;

  if ($isAry && $else !== null && count($v) === 0) {
   $ret = $else($cx, $in);
   return $ret;
  }

  // #var, detect input type is object or not
  if (!$loop && $isAry) {
   $keys = array_keys($v);
   $loop = (count(array_diff_key($v, array_keys($keys))) == 0);
   $isObj = !$loop;
  }

  if (($loop && $isAry) || $isTrav) {
   if ($each && !$isTrav) {
    // Detect input type is object or not when never done once
    if ($keys == null) {
     $keys = array_keys($v);
     $isObj = (count(array_diff_key($v, array_keys($keys))) > 0);
    }
   }
   $ret = array();
   if ($push) {
    $cx['scopes'][] = $in;
   }
   $i = 0;
   if ($cx['flags']['spvar']) {
    $old_spvar = $cx['sp_vars'];
    $cx['sp_vars'] = array_merge(array('root' => $old_spvar['root']), $old_spvar, array('_parent' => $old_spvar));
    if (!$isTrav) {
     $last = count($keys) - 1;
    }
   }

   $isSparceArray = $isObj && (count(array_filter(array_keys($v), 'is_string')) == 0);
   foreach ($v as $index => $raw) {
    if ($cx['flags']['spvar']) {
     $cx['sp_vars']['first'] = ($i === 0);
     $cx['sp_vars']['last'] = ($i == $last);
     $cx['sp_vars']['key'] = $index;
     $cx['sp_vars']['index'] = $isSparceArray ? $index : $i;
     $i++;
    }
    if (isset($bp[0])) {
     $raw = lcr58dbd9e30e392m($cx, $raw, array($bp[0] => $raw));
    }
    if (isset($bp[1])) {
     $raw = lcr58dbd9e30e392m($cx, $raw, array($bp[1] => $cx['sp_vars']['index']));
    }
    $ret[] = $cb($cx, $raw);
   }
   if ($cx['flags']['spvar']) {
    if ($isObj) {
     unset($cx['sp_vars']['key']);
    } else {
     unset($cx['sp_vars']['last']);
    }
    unset($cx['sp_vars']['index']);
    unset($cx['sp_vars']['first']);
    $cx['sp_vars'] = $old_spvar;
   }
   if ($push) {
    array_pop($cx['scopes']);
   }
   return join('', $ret);
  }
  if ($each) {
   if ($else !== null) {
    $ret = $else($cx, $v);
    return $ret;
   }
   return '';
  }
  if ($isAry) {
   if ($push) {
    $cx['scopes'][] = $in;
   }
   $ret = $cb($cx, $v);
   if ($push) {
    array_pop($cx['scopes']);
   }
   return $ret;
  }

  if ($v === true) {
   return $cb($cx, $in);
  }

  if (($v !== null) && ($v !== false)) {
   return $cb($cx, $v);
  }

  if ($else !== null) {
   $ret = $else($cx, $in);
   return $ret;
  }

  return '';
 }}

 if (!function_exists('lcr58dbd9e30e392ifvar')) { function lcr58dbd9e30e392ifvar($cx, $v, $zero) {
  return ($v !== null) && ($v !== false) && ($zero || ($v !== 0) && ($v !== 0.0)) && ($v !== '') && (is_array($v) ? (count($v) > 0) : true);
 }}

 if (!function_exists('lcr58dbd9e30e392raw')) { function lcr58dbd9e30e392raw($cx, $v, $ex = 0) {
  if ($ex) {
   return $v;
  }

  if ($v === true) {
   if ($cx['flags']['jstrue']) {
    return 'true';
   }
  }

  if (($v === false)) {
   if ($cx['flags']['jstrue']) {
    return 'false';
   }
  }

  if (is_array($v)) {
   if ($cx['flags']['jsobj']) {
    if (count(array_diff_key($v, array_keys(array_keys($v)))) > 0) {
     return '[object Object]';
    } else {
     $ret = array();
     foreach ($v as $k => $vv) {
      $ret[] = lcr58dbd9e30e392raw($cx, $vv);
     }
     return join(',', $ret);
    }
   } else {
    return 'Array';
   }
  }

  return "$v";
 }}

 if (!function_exists('lcr58dbd9e30e392m')) { function lcr58dbd9e30e392m($cx, $a, $b) {
  if (is_array($b)) {
   if ($a === null) {
    return $b;
   } else if (is_array($a)) {
    return array_merge($a, $b);
   } else if (($cx['flags']['method'] || $cx['flags']['prop']) && is_object($a)) {
    foreach ($b as $i => $v) {
     $a->$i = $v;
    }
   }
  }
  return $a;
 }}

if (!class_exists("LS")) {
class LS {
 public static $jsContext = array (
  'flags' => 
  array (
    'jstrue' => 1,
    'jsobj' => 1,
  ),
);
    public function __construct($str, $escape = false) {
        $this->string = $escape ? (($escape === 'encq') ? static::encq(static::$jsContext, $str) : static::enc(static::$jsContext, $str)) : $str;
    }
    public function __toString() {
        return $this->string;
    }
    public static function stripExtendedComments($template) {
        return preg_replace(static::EXTENDED_COMMENT_SEARCH, '{{! }}', $template);
    }
    public static function escapeTemplate($template) {
        return addcslashes(addcslashes($template, '\\'), "'");
    }
    public static function raw($cx, $v, $ex = 0) {
        if ($ex) {
            return $v;
        }

        if ($v === true) {
            if ($cx['flags']['jstrue']) {
                return 'true';
            }
        }

        if (($v === false)) {
            if ($cx['flags']['jstrue']) {
                return 'false';
            }
        }

        if (is_array($v)) {
            if ($cx['flags']['jsobj']) {
                if (count(array_diff_key($v, array_keys(array_keys($v)))) > 0) {
                    return '[object Object]';
                } else {
                    $ret = array();
                    foreach ($v as $k => $vv) {
                        $ret[] = static::raw($cx, $vv);
                    }
                    return join(',', $ret);
                }
            } else {
                return 'Array';
            }
        }

        return "$v";
    }
    public static function enc($cx, $var) {
        return htmlspecialchars(static::raw($cx, $var), ENT_QUOTES, 'UTF-8');
    }
    public static function encq($cx, $var) {
        return str_replace(array('=', '`', '&#039;'), array('&#x3D;', '&#x60;', '&#x27;'), htmlspecialchars(static::raw($cx, $var), ENT_QUOTES, 'UTF-8'));
    }
}
}
return function ($in = null, $options = null) {
    $helpers = array();
    $partials = array();
    $cx = array(
        'flags' => array(
            'jstrue' => true,
            'jsobj' => true,
            'jslen' => true,
            'spvar' => true,
            'prop' => false,
            'method' => false,
            'lambda' => false,
            'mustlok' => false,
            'mustlam' => false,
            'echo' => true,
            'partnc' => false,
            'knohlp' => false,
            'debug' => isset($options['debug']) ? $options['debug'] : 1,
        ),
        'constants' =>  array(
            'DEBUG_ERROR_LOG' => 1,
            'DEBUG_ERROR_EXCEPTION' => 2,
            'DEBUG_TAGS' => 4,
            'DEBUG_TAGS_ANSI' => 12,
            'DEBUG_TAGS_HTML' => 20,
        ),
        'helpers' => isset($options['helpers']) ? array_merge($helpers, $options['helpers']) : $helpers,
        'partials' => isset($options['partials']) ? array_merge($partials, $options['partials']) : $partials,
        'scopes' => array(),
        'sp_vars' => isset($options['data']) ? array_merge(array('root' => $in), $options['data']) : array('root' => $in),
        'blparam' => array(),
        'partialid' => 0,
        'runtime' => '\LightnCandy\Runtime',
    );
    
    ob_start();echo '
<div id="search_results" class="container">

	<h2>',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['search_title'])) ? $in['search_title'] : null)),'</h2>

	<div id="keywords">
		<h3>',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['search_text'])) ? $in['search_text'] : null)),': <span class="keyword">',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['query'])) ? $in['query'] : null)),'</span></h3>
	</div>

	<h4>',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['search_tutorial_title'])) ? $in['search_tutorial_title'] : null)),'</h4>

	<div class="row">

',lcr58dbd9e30e392sec($cx, ((is_array($in) && isset($in['posts'])) ? $in['posts'] : null), null, $in, true, function($cx, $in) {echo '
';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['sfwd-lessons'])) ? $in['sfwd-lessons'] : null), false)){echo '
';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['even'])) ? $in['even'] : null), false)){echo '					</div><div class="row">
';}else{echo '';}echo '
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 tutorial-result-container">

					<div class="col-md-6 col-sm-4 col-xs-6">

';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['thumbnail'])) ? $in['thumbnail'] : null), false)){echo '							<a href="',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['permalink'])) ? $in['permalink'] : null)),'" ';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['time'])) ? $in['time'] : null), false)){echo ' class="playable-content" ';}else{echo '';}echo '>
								<img src="',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['thumbnail'])) ? $in['thumbnail'] : null)),'" class="play-img thumbnail">
							</a>
';}else{echo '';}echo '
					</div>

					<div class="col-md-6 col-sm-8 col-xs-6 un-search-box">

						<p class="link"><a href="',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['permalink'])) ? $in['permalink'] : null)),'">',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['post_title'])) ? $in['post_title'] : null)),'';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['time'])) ? $in['time'] : null), false)){echo ' - ',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['time'])) ? $in['time'] : null)),'';}else{echo '';}echo '</a></p>

';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['intro'])) ? $in['intro'] : null), false)){echo '							<div class="excerpt">',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['intro'])) ? $in['intro'] : null)),'</div>
';}else{echo '';}echo '
					</div>

				</div>

';}else{echo '';}echo '
';}),'
	</div>

	<div class="row course">

		<h4>',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['search_course_title'])) ? $in['search_course_title'] : null)),'</h4>

',lcr58dbd9e30e392sec($cx, ((is_array($in) && isset($in['posts'])) ? $in['posts'] : null), null, $in, true, function($cx, $in) {echo '
';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['sfwd-courses'])) ? $in['sfwd-courses'] : null), false)){echo '
				<div class="col-sm-3 col-xs-6 un-search-box-tutorial">

					<div class="tdcourse">

';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['thumbnail'])) ? $in['thumbnail'] : null), false)){echo '							<p>

';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['thumbnail'])) ? $in['thumbnail'] : null), false)){echo '								<a href="',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['permalink'])) ? $in['permalink'] : null)),'">
';}else{echo '';}echo '
							<img src="',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['thumbnail'])) ? $in['thumbnail'] : null)),'" class="thumbnailcourse"></p>

';if (lcr58dbd9e30e392ifvar($cx, ((is_array($in) && isset($in['thumbnail'])) ? $in['thumbnail'] : null), false)){echo '								</a>
';}else{echo '';}echo '
							</p>

';}else{echo '';}echo '
						<p class="linkcourse t"><a href="',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['permalink'])) ? $in['permalink'] : null)),'">',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['post_title'])) ? $in['post_title'] : null)),'</a></p>
						<a class="course-info-xs hidden-lg hidden-md" href="',lcr58dbd9e30e392encq($cx, ((is_array($in) && isset($in['permalink'])) ? $in['permalink'] : null)),'">',lcr58dbd9e30e392encq($cx, ((isset($in['bound_lessons']) && is_array($in['bound_lessons']) && isset($in['bound_lessons']['length'])) ? $in['bound_lessons']['length'] : ((isset($in['bound_lessons']) && is_array($in['bound_lessons'])) ? count($in['bound_lessons']) : null))),' videos | view course ></a>

					</div>

					<br />

				</div>

';}else{echo '';}echo '
';}),'
	</div>

</div>
';return ob_get_clean();
};?>