<?php  if (!function_exists('lcr596cd02b4c5cfsec')) { function lcr596cd02b4c5cfsec($cx, $v, $bp, $in, $each, $cb, $else = null) {
  $push = ($in !== $v) || $each;

  $isAry = is_array($v) || ($v instanceof \ArrayObject);
  $isTrav = $v instanceof \Traversable;
  $loop = $each;
  $keys = null;
  $last = null;
  $isObj = false;

  if ($isAry && $else !== null && count($v) === 0) {
   $ret = $else($cx, $in);
   return $ret;
  }

  // #var, detect input type is object or not
  if (!$loop && $isAry) {
   $keys = array_keys($v);
   $loop = (count(array_diff_key($v, array_keys($keys))) == 0);
   $isObj = !$loop;
  }

  if (($loop && $isAry) || $isTrav) {
   if ($each && !$isTrav) {
    // Detect input type is object or not when never done once
    if ($keys == null) {
     $keys = array_keys($v);
     $isObj = (count(array_diff_key($v, array_keys($keys))) > 0);
    }
   }
   $ret = array();
   if ($push) {
    $cx['scopes'][] = $in;
   }
   $i = 0;
   if ($cx['flags']['spvar']) {
    $old_spvar = $cx['sp_vars'];
    $cx['sp_vars'] = array_merge(array('root' => $old_spvar['root']), $old_spvar, array('_parent' => $old_spvar));
    if (!$isTrav) {
     $last = count($keys) - 1;
    }
   }

   $isSparceArray = $isObj && (count(array_filter(array_keys($v), 'is_string')) == 0);
   foreach ($v as $index => $raw) {
    if ($cx['flags']['spvar']) {
     $cx['sp_vars']['first'] = ($i === 0);
     $cx['sp_vars']['last'] = ($i == $last);
     $cx['sp_vars']['key'] = $index;
     $cx['sp_vars']['index'] = $isSparceArray ? $index : $i;
     $i++;
    }
    if (isset($bp[0])) {
     $raw = lcr596cd02b4c5cfm($cx, $raw, array($bp[0] => $raw));
    }
    if (isset($bp[1])) {
     $raw = lcr596cd02b4c5cfm($cx, $raw, array($bp[1] => $cx['sp_vars']['index']));
    }
    $ret[] = $cb($cx, $raw);
   }
   if ($cx['flags']['spvar']) {
    if ($isObj) {
     unset($cx['sp_vars']['key']);
    } else {
     unset($cx['sp_vars']['last']);
    }
    unset($cx['sp_vars']['index']);
    unset($cx['sp_vars']['first']);
    $cx['sp_vars'] = $old_spvar;
   }
   if ($push) {
    array_pop($cx['scopes']);
   }
   return join('', $ret);
  }
  if ($each) {
   if ($else !== null) {
    $ret = $else($cx, $v);
    return $ret;
   }
   return '';
  }
  if ($isAry) {
   if ($push) {
    $cx['scopes'][] = $in;
   }
   $ret = $cb($cx, $v);
   if ($push) {
    array_pop($cx['scopes']);
   }
   return $ret;
  }

  if ($v === true) {
   return $cb($cx, $in);
  }

  if (($v !== null) && ($v !== false)) {
   return $cb($cx, $v);
  }

  if ($else !== null) {
   $ret = $else($cx, $in);
   return $ret;
  }

  return '';
 }}

 if (!function_exists('lcr596cd02b4c5cfifvar')) { function lcr596cd02b4c5cfifvar($cx, $v, $zero) {
  return ($v !== null) && ($v !== false) && ($zero || ($v !== 0) && ($v !== 0.0)) && ($v !== '') && (is_array($v) ? (count($v) > 0) : true);
 }}

 if (!function_exists('lcr596cd02b4c5cfencq')) { function lcr596cd02b4c5cfencq($cx, $var) {
  if ($var instanceof LS) {
   return (string)$var;
  }

  return str_replace(array('=', '`', '&#039;'), array('&#x3D;', '&#x60;', '&#x27;'), htmlspecialchars(lcr596cd02b4c5cfraw($cx, $var), ENT_QUOTES, 'UTF-8'));
 }}

 if (!function_exists('lcr596cd02b4c5cfm')) { function lcr596cd02b4c5cfm($cx, $a, $b) {
  if (is_array($b)) {
   if ($a === null) {
    return $b;
   } else if (is_array($a)) {
    return array_merge($a, $b);
   } else if (($cx['flags']['method'] || $cx['flags']['prop']) && is_object($a)) {
    foreach ($b as $i => $v) {
     $a->$i = $v;
    }
   }
  }
  return $a;
 }}

 if (!function_exists('lcr596cd02b4c5cfraw')) { function lcr596cd02b4c5cfraw($cx, $v, $ex = 0) {
  if ($ex) {
   return $v;
  }

  if ($v === true) {
   if ($cx['flags']['jstrue']) {
    return 'true';
   }
  }

  if (($v === false)) {
   if ($cx['flags']['jstrue']) {
    return 'false';
   }
  }

  if (is_array($v)) {
   if ($cx['flags']['jsobj']) {
    if (count(array_diff_key($v, array_keys(array_keys($v)))) > 0) {
     return '[object Object]';
    } else {
     $ret = array();
     foreach ($v as $k => $vv) {
      $ret[] = lcr596cd02b4c5cfraw($cx, $vv);
     }
     return join(',', $ret);
    }
   } else {
    return 'Array';
   }
  }

  return "$v";
 }}

if (!class_exists("LS")) {
class LS {
 public static $jsContext = array (
  'flags' => 
  array (
    'jstrue' => 1,
    'jsobj' => 1,
  ),
);
    public function __construct($str, $escape = false) {
        $this->string = $escape ? (($escape === 'encq') ? static::encq(static::$jsContext, $str) : static::enc(static::$jsContext, $str)) : $str;
    }
    public function __toString() {
        return $this->string;
    }
    public static function stripExtendedComments($template) {
        return preg_replace(static::EXTENDED_COMMENT_SEARCH, '{{! }}', $template);
    }
    public static function escapeTemplate($template) {
        return addcslashes(addcslashes($template, '\\'), "'");
    }
    public static function raw($cx, $v, $ex = 0) {
        if ($ex) {
            return $v;
        }

        if ($v === true) {
            if ($cx['flags']['jstrue']) {
                return 'true';
            }
        }

        if (($v === false)) {
            if ($cx['flags']['jstrue']) {
                return 'false';
            }
        }

        if (is_array($v)) {
            if ($cx['flags']['jsobj']) {
                if (count(array_diff_key($v, array_keys(array_keys($v)))) > 0) {
                    return '[object Object]';
                } else {
                    $ret = array();
                    foreach ($v as $k => $vv) {
                        $ret[] = static::raw($cx, $vv);
                    }
                    return join(',', $ret);
                }
            } else {
                return 'Array';
            }
        }

        return "$v";
    }
    public static function enc($cx, $var) {
        return htmlspecialchars(static::raw($cx, $var), ENT_QUOTES, 'UTF-8');
    }
    public static function encq($cx, $var) {
        return str_replace(array('=', '`', '&#039;'), array('&#x3D;', '&#x60;', '&#x27;'), htmlspecialchars(static::raw($cx, $var), ENT_QUOTES, 'UTF-8'));
    }
}
}
return function ($in = null, $options = null) {
    $helpers = array();
    $partials = array();
    $cx = array(
        'flags' => array(
            'jstrue' => true,
            'jsobj' => true,
            'spvar' => true,
            'prop' => false,
            'method' => false,
            'lambda' => false,
            'mustlok' => false,
            'mustlam' => false,
            'echo' => true,
            'partnc' => false,
            'knohlp' => false,
            'debug' => isset($options['debug']) ? $options['debug'] : 1,
        ),
        'constants' =>  array(
            'DEBUG_ERROR_LOG' => 1,
            'DEBUG_ERROR_EXCEPTION' => 2,
            'DEBUG_TAGS' => 4,
            'DEBUG_TAGS_ANSI' => 12,
            'DEBUG_TAGS_HTML' => 20,
        ),
        'helpers' => isset($options['helpers']) ? array_merge($helpers, $options['helpers']) : $helpers,
        'partials' => isset($options['partials']) ? array_merge($partials, $options['partials']) : $partials,
        'scopes' => array(),
        'sp_vars' => isset($options['data']) ? array_merge(array('root' => $in), $options['data']) : array('root' => $in),
        'blparam' => array(),
        'partialid' => 0,
        'runtime' => '\LightnCandy\Runtime',
    );
    
    ob_start();echo '<div class="qb-wrapper">
  <div class="qb-search-page">    
    <div class="qb-background-gray">
      <div class="qb-jobType-component">
        <div class="container">
          <div class="row">
',lcr596cd02b4c5cfsec($cx, ((is_array($in) && isset($in['posts'])) ? $in['posts'] : null), null, $in, true, function($cx, $in) {echo '';if (lcr596cd02b4c5cfifvar($cx, ((is_array($in) && isset($in['sfwd-lessons'])) ? $in['sfwd-lessons'] : null), false)){echo '					<div class="col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-6 col-md-4 col-lg-12">
		              <div class="qb-jobType-video-item">
		                <a href="',lcr596cd02b4c5cfencq($cx, ((is_array($in) && isset($in['permalink'])) ? $in['permalink'] : null)),'">
		                  <div class="qb-jobType-video-thumb">
		                    <div class="overlay"></div>
		                    <div class="preview" style="background: url(',lcr596cd02b4c5cfencq($cx, ((is_array($in) && isset($in['thumbnail'])) ? $in['thumbnail'] : null)),'); background-size: cover;"></div>
		                  </div>
		                  <div class="qb-jobType-video-meta">
		                    <h4>',lcr596cd02b4c5cfencq($cx, ((is_array($in) && isset($in['post_title'])) ? $in['post_title'] : null)),'</h4>
		                    <span>';if (lcr596cd02b4c5cfifvar($cx, ((is_array($in) && isset($in['time'])) ? $in['time'] : null), false)){echo '',lcr596cd02b4c5cfencq($cx, ((is_array($in) && isset($in['time'])) ? $in['time'] : null)),'';}else{echo '';}echo '</span>
		                  </div>
		                </a>
		              </div>
		            </div>
';}else{echo '';}echo '';}),'			<div id="show_more_div" class="col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-12 col-md-12 col-lg-12"
				';if (lcr596cd02b4c5cfifvar($cx, ((is_array($in) && isset($in['only_one_page'])) ? $in['only_one_page'] : null), false)){echo ' style="display:none;" ';}else{echo '';}echo '
			>
				<div class="col-md-12"><a class="qb-see-more show_more_search" href="#">See more</a></div>
			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

	var search_pagination = {
		key: \'',lcr596cd02b4c5cfencq($cx, ((is_array($in) && isset($in['query'])) ? $in['query'] : null)),'\',
		page : 1
	};

	$(document).ready(function(){
		
		$(\'.show_more_search\').click(function(){
			var new_items = \'\';
			var new_item = \'\';
			
			search_pagination.page++;
			
			$.get( "',lcr596cd02b4c5cfencq($cx, ((is_array($in) && isset($in['service_url'])) ? $in['service_url'] : null)),'", search_pagination, function( data ) {
			  	
			  	if(data.max_num_pages == search_pagination.page){
			  		$(\'.show_more_search\').hide();
			  	}
			  	  
				$.each( data.posts, function( key, aPost ) {
				    var new_item = \'\';
					new_item += \'<div class="col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-6 col-md-4 col-lg-12">\';
					new_item += \'<div class="qb-jobType-video-item">\';
				    new_item += \'<a href="\'+ aPost.permalink + \'">\';
				    new_item += \'<div class="qb-jobType-video-thumb">\';
				    new_item += \'<div class="overlay"></div>\';
				    new_item += \'<div class="preview" style="background: url(\'+ aPost.thumbnail + \'); background-size: cover;"></div>\';
				    new_item += \'</div>\';
				    new_item += \'<div class="qb-jobType-video-meta">\';
				    new_item += \'<h4>\'+ aPost.post_title + \'</h4>\';
				    new_item += \'<span>\'+ aPost.time + \'</span>\';
				    new_item += \'</div>\';
				    new_item += \'</a>\';
				    new_item += \'</div>\';
				    new_item += \'</div>\';
					new_items += new_item;
				});
				
				$(new_items).insertBefore( "#show_more_div" );
				
			});
		});
	});
</script>';return ob_get_clean();
};?>