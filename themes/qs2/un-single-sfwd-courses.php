<?php 
# get the lessons in the course
$bound_lessons = get_lessons_of_course($current_id);

$course = get_post($current_id);

# get the permalink & remove swfd-lesson from permalink
$plink = get_the_permalink($bound_lessons[0]->ID);

# update meta for use in lesson template
update_post_meta( $bound_lessons[0]->ID, 'course_parent', $current_id );

# redirect the course to the first lesson
// header("Location: ".$plink );
?>
<div key="loader" className="loader-wrapper">
	<div className="loader"></div>
</div>
<script>
	localStorage.setItem("course-slug", "<?php echo $course->post_name; ?>");
	window.location.href = "<?php echo $plink ?>";
</script>