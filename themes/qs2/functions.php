<?php

/*----------------------------------------------------*/
// Hook for save metadata in lessons about the parent
/*----------------------------------------------------*/
function save_lessons_course_meta( $post_id, $post, $update )
{
	if($post->post_type == 'sfwd-courses')
	{
		if (isset($_POST['acf']['field_563a6ee7b83cd']))
		{
			$lessons = $_POST['acf']['field_563a6ee7b83cd'];
			foreach ($lessons as $lesson) {
				if ( ! add_post_meta( $lesson, 'course_parent', $post->ID, true ) ) { 
					update_post_meta( $lesson, 'course_parent', $post->ID );
				}				
			}
		}
	}
}
add_action( 'save_post', 'save_lessons_course_meta', 10, 3 );

/*----------------------------------------------------*/
// Register child theme view paths.
/*----------------------------------------------------*/
add_filter('themosisViewPaths', function($paths)
{
    $paths[] = get_stylesheet_directory().'/resources/views/';
    return $paths;
});

/*----------------------------------------------------*/
// Register child theme asset paths.
/*----------------------------------------------------*/
add_filter('themosisAssetPaths', function($paths)
{
    $url = get_stylesheet_directory_uri().'/app/assets'; // URL to your child theme assets directory

    $paths[$url] = get_stylesheet_directory().'/app/assets';
    return $paths;
});

# load all of the *child* vc dependencies, in order
require_once( dirname( __FILE__ ) . '/resources/components/vc-intuit-qs/load.php');

# load plugin change /custom-post/slug to /slug
// require_once( dirname( __FILE__ ) . '/resources/components/remove-slug/remove-slug-custom-post-type.php');

# load all of the *child* admin dependencies, in order
$files = glob(get_stylesheet_directory() . '/resources/admin/*.php');
foreach ($files as $file) {
    require_once($file);   
}

function qs2_scripts() {
    wp_enqueue_script( 'qs2-search', dirname(get_stylesheet_uri()) . '/resources/assets/js/search.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'qs2-header', dirname(get_stylesheet_uri()) . '/resources/assets/js/header.js', array('jquery'), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'qs2_scripts' );
