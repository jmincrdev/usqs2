jQuery(document).ready(function(){
	
	console.log("Using wizard-qs2.js: JSONP QS2 version");
	
	var validAPI = false;
	var wpPostTypes;
	var APIpostsPerPage = 100; 			//max and rec is 100
	var APIpage = 1; 					//pagination 
	var APIRequestPostCount = 0; 		//the number of posts in the last query
	var APIendpoint = "local";			// our current API
	var query;							// our last API response
	
	var cacheTTL = 300;					// cache requests for 5 minutes (300 seconds)
	var wpPosts = []; 					// this is the post database that we build and store in local storage
										// we add an extra key (api_source) to the REST API response to indicate what WP install the post belongs to
	
	var wpAuthors = [];					// we add an extra key (api_source) to the REST API response to indicate what WP install the author belongs to
	var wpCategories = [];				// we add an extra key (api_source) to the REST API response to indicate what WP install the category belongs to
	var wpTags = [];					// we add an extra key (api_source) to the REST API response to indicate what WP install the tag belongs to
	
	//var selectedPosts = []; 			// the posts we have selected
	var dataTable;						// our global datatable
	
	<!-- helper functions -->
	function WPinit(){
		jQuery('#customAPI').hide();
		
		dataTable = jQuery('#postTable').DataTable( {
												dom: 'Bfrtip',
												deferRender: true,
												select: {
													style: 'multi'
												},
												buttons: [
													{
														text: 'Export posts',
														action: function () {

															var auth = "Basic " + btoa( jQuery("#user").val() + ":" + jQuery("#pass").val() );
															
															var out = dataTable.rows( { selected: true } ).data();
															
															delete out['context'];
															delete out['selector'];
															delete out['ajax'];
															
															var page = "json-migrate"
															var action = "migrate.migration";
															
															// get the custom url if needed
															if (APIendpoint=="local" && jQuery("#APIServer").val()) {
																APIendpoint = jQuery("#APIServer").val();
															}
															var api = encodeURIComponent(APIendpoint);															
															var key = encodeURIComponent(auth);
															var posts = "";

        													for (var i = 0; i < out.length; i++) {
																posts = posts + out[i][0] + ',' + out[i][6] + ',' + out[i][7] + ';';
															}
															
															url = adminUrl + "?page=" + page + "&action=" + action;

															var form = jQuery('<form action="' + url + '" method="post">' +
															  '<input type="text" name="api" value="' + api + '" />' +
															  '<input type="text" name="key" value="' + key + '" />' +
															  '<input type="text" name="posts" value="' + posts + '" />' +
															  '</form>');
															jQuery('body').append(form);
															form.submit();

														}
													}
												]
												
											} );
		
		// get our loc
		jQuery.ajax( {
		  url: jsonFile,
		  success: function ( data, res, req ) {
			var out = "<select id='activeSites'><option value='"+window.location.origin+"'>Local environment</option>";
			
			// add in our local json options
			if(res === "success"){
				data.forEach( function (arrayItem){
					out = out + '<option value="'+arrayItem.url+'">'+arrayItem.name+'</option>';
				});
			}
			
			// append the select tool
			out = out + "<option value='custom'>Custom endpoint</option></select>"
			jQuery('#sites').append(out);
			
		  },
		  cache: false
		} );
		
		// load our data from jstorage into wpPosts
		
	}
	
	//applies post data to the form
	function applyPosts(){		
	
		// clear the data table
		dataTable.clear().draw();

		wpPosts.forEach( function (arrayItem){
			
			var postCats = "";
			//if(arrayItem.categories !== undefined) postCats = arrayItem.categories.join();
			
			var postTags = "";
			//postTags = arrayItem.tags;
			//if (typeof array[postTags] === 'undefined') postTags = "";
			
			dataTable.row.add( [
				arrayItem.id,
				'<a href="'+arrayItem.link+'" target="_blank">' + arrayItem.title.rendered + '</a>',
				arrayItem.author || '',
				postCats || '',
				arrayItem.date,
				postTags || '',
				arrayItem.type,
				arrayItem.status
			] ).draw( false );

		});
	}
	
	//applies post type data to the form
	function applyPostTypes(){
		
		jQuery('#postTypeGroup').empty();
		
		wpPostTypes.forEach( function (arrayItem){

			// ignore tasks, they will be imported with courses
			var links = jQuery.map(arrayItem._links, function(value, index) {
				return [value];
			});

			// keep the link in the domain given
			var link = jQuery("#APIServer").val() + links[1][0]['href'].substring(links[1][0]['href'].indexOf("/wp-json"));
			
			var the_name = arrayItem.name;
			if (the_name=="Tasks") {
				the_name = "Tasks and Featured Media";
			}
			if (the_name=="Courses") {
				the_name = "Courses, Tasks and Featured Media";
			}
			
			jQuery('#postTypeGroup').append('<div class="radio"><label><input class="cpt" type="radio" name="postType" value="'+link+'">'+the_name+'</label></div>');
			
		});
	}
	
	//get the posts
	function wp_get_posts(APIpath,filters,clearCache,applyPostsFlag){
		
		// check if in jstorage
		
		// are we supposed to bust the cache?
		
		if(clearCache != false){
			wp_get_request(APIpath,filters);
		}
	}
	
	//get the authors
	function wp_get_authors(APIpath,filters,clearCache){

		// check if in jstorage
		
		// are we supposed to bust the cache?
		
		if(clearCache != false){
			
				
				jQuery.when(wp_get_request(APIpath,filters)).done(function(){
						
						// add our query to wpPosts
						wpPosts = wpPosts.concat(query);
						
						// check to see if we need to query more
						while(APIRequestPostCount > (APIpostsPerPage * APIpage) ){
						
							APIpage++;
						
							var filter = "?per_page="+APIpostsPerPage+"&page="+APIpage;
							jQuery.when(wp_get_request(APIpath,filter)).done(function(){
									wpPosts = wpPosts.concat(query);
									applyPosts();
									
							});
						}
						
						// update the UX
						applyPosts();

				});
			//}
		}
	}
	
	
	// the wrapper function to make a REST API request
	function wp_get_request(APIpath,filters) {
		
		//add in the active filters
		filters = filters + "&status=" + jQuery("#postStatus").val();

		var functionName = doJsonpCall(APIpath + filters);
		window[functionName] = function( data, res, req ) {
			
			//APIRequestPostCount = req.getResponseHeader('X-WP-Total');
			
			//wpPosts = [];
			var temp = [];
			for (var p in data){
				// add the source of the request to the post object for sanity
				data[p]["api_source"] = APIpath;
				temp.push(data[p]);
			}
			query = temp;

			// keep track of the object status
			for (var i = 0; i < query.length; i++) {
				query[i].status=jQuery('#postStatus').val();
			}

			// add our query to wpPosts
			wpPosts = wpPosts.concat(query);
		
			// update the UX
			applyPosts();
			
		}

	}
	
	
	/*
	 * Add a parameter ?paramName=Value or &paramName=Value to an url
	 */
	 
	function setGetParameter(url, paramName, paramValue){
		if (url.indexOf(paramName + "=") >= 0)
		{
			var prefix = url.substring(0, url.indexOf(paramName));
			var suffix = url.substring(url.indexOf(paramName));
			suffix = suffix.substring(suffix.indexOf("=") + 1);
			suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
			url = prefix + paramName + "=" + paramValue + suffix;
		}
		else
		{
			if (url.indexOf("?") < 0)
				url += "?" + paramName + "=" + paramValue;
			else
				url += "&" + paramName + "=" + paramValue;
		}
		return url;
	}

	/*
	 * This makes a JSONP ajax call to the WP REST API V2 and return the callback function name
	 */

	function doJsonpCall(baseurl) {

		var functionName = 'd' + Math.floor(Math.random()*1000001);

		var url = setGetParameter(baseurl, "_jsonp", functionName);
		var url = setGetParameter(url, "callback", "JSON_CALLBACK");

		jQuery.ajax({
			type: 'GET',
			url: url,
			dataType: 'jsonp',
			contentType: "application/json"
		});
		
		return functionName;
		
	}

	//validate API
	function wp_valid_api(APIendpoint){	
	
		var functionName = doJsonpCall(APIendpoint + '/wp-json');
		
		window[functionName] = function(posts){
			jQuery("#customAPIResult").show().delay(5000).fadeOut();
		}
	
	}
	
	
	//get post types and return as array
	function wp_get_post_types(APIendpoint){
		
		var functionName = doJsonpCall(APIendpoint + '/wp-json/wp/v2/types');

		window[functionName] = function(data){

			wpPostTypes = [];
			for (var p in data){
			  wpPostTypes.push(data[p]);
			}
			applyPostTypes();

		}

	}

	<!--- the actual step controller -->
	var formStep = jQuery("#stepForm");
	formStep.validate({
		errorPlacement: function errorPlacement(error, element) { element.before(error); },
		rules: {
			confirm: {
				//equalTo: "#password"
			}
		}
	});
	formStep.children("div").steps({
		headerTag: "h3",
		bodyTag: "section",
		transitionEffect: "slideLeft",
		onStepChanging: function (event, currentIndex, newIndex)
		{
			// Always allow previous action even if the current form is not valid!
			if (currentIndex > newIndex)    return true;

			// check to see if step1 has a valid API server
			if (currentIndex === 0){
				
				jQuery.when( wp_valid_api( jQuery("#APIServer").val() ) ).done(function(){
					if( validAPI ){
						APIpage = 1;
						return true;
					}else{
						return false;
					}
				});
			}
			
			formStep.validate().settings.ignore = ":disabled,:hidden";
			return formStep.valid();
		},
		onStepChanged: function (event, currentIndex, priorIndex)
		{
			if (currentIndex === 1){
				wp_get_post_types( jQuery("#APIServer").val() );
			}
		},
		onFinishing: function (event, currentIndex)
		{
			formStep.validate().settings.ignore = ":disabled,:hidden";
			return formStep.valid();
		},
		onFinished: function (event, currentIndex)
		{
			// simulate a button click on the data table :)
			jQuery("#postTable_wrapper").find(".dt-button").click();
		}
	});
	
	<!-- DOC listeners -->
	jQuery(function() {
		
		WPinit();
	
		// api endpoint
		jQuery("#sites").on("change", "#activeSites", function(){
			
			jQuery('#APIServer').val( jQuery('#activeSites').val() );
			
			if(jQuery(this).val() === "custom" ){
				jQuery('#customAPI').show();
			}else{
				APIendpoint = jQuery(this).val();
				jQuery('#customAPI').hide();
			}
		});
		
		// custom api binding to APIendpoint
		jQuery("#customAPI").on("change", "#APIserver", function(){
				APIendpoint = jQuery(this).val();
		});
		
	
		// validate
		jQuery("#validateAPI").click(function(e){
			e.preventDefault();
			e.stopPropagation();
			
			jQuery.when( wp_valid_api( jQuery("#APIServer").val() ) ).done(function(){
			});
		});
		
		
		// post type radio buttons
		jQuery("#postTypeGroup").on("click", ".radio", function(e){
			e.preventDefault();
			e.stopPropagation();
			
			jQuery(this).find(".cpt").prop("checked", !jQuery(this).prop("checked"));
			
			// clear the active displayed table
			dataTable.clear().draw();
			
			// reset the pagination
			APIpage = 1;
			var APIpath = jQuery(this).find(".cpt").val();

			wp_get_posts( APIpath, "?per_page="+APIpostsPerPage+"&page="+APIpage);
			
		});	
	});		

});
