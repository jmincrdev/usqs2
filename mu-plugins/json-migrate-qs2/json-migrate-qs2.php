<?php
/*
Plugin Name: JSON Migrate QS2
Description: JSON content migrator plugin for Quickstart V2
Version: 1.0
*/

function json_migrate_after_post_create_hook($id, $wpObj, $url_api) {

	// if its a lesson 
	if ($wpObj["type"] == "sfwd-lessons") {

		// better delete the post and start again
		wp_delete_post($id, true);
		$lesson_id = json_migrate_save_lesson($wpObj["id"], $url_api);

		if($lesson_id){
			echo "Lesson ID: #".$lesson_id."<br>" . PHP_EOL;
		}else{
			echo "<span style=\"color: red;\">FAILURE for " . $lesson_id . " </span><br>" . PHP_EOL;
		}
		
	}

	// if its a course import all the lessons and media
	if ($wpObj["type"] == "sfwd-courses") {
	
		$wpData = new wpData();

		/* Save ACFs: excerpt, course_text */ 
		update_field("excerpt", $wpObj["acf"]["excerpt"], $id);
		update_field("course_text", $wpObj["acf"]["course_text"], $id);
		
		/* import transparent image */
		if ($wpObj["acf"]["transparent_image"]) {
			
			/* save transparent media */
			$id_img = $wpData->uploadImage($wpObj["acf"]["transparent_image"]);

			/* assign media id as transparent media */
			if ($id_img) update_field("transparent_image", $id_img, $id);
			
		}

		/* 	Cicle [bound_lessons] */
		if ($wpObj["acf"]["bound_lessons"]) {
		
			 $bound_lessons=[];
		
			foreach($wpObj["acf"]["bound_lessons"] as $lesson) {

				$lesson_id = json_migrate_save_lesson($lesson["ID"], $url_api);

				if($lesson_id){
					echo "Lesson ID: #".$lesson_id."<br>" . PHP_EOL;
				}else{
					echo "<span style=\"color: red;\">FAILURE for " . $lesson_id . " </span><br>" . PHP_EOL;
				}

			}

			/* add the array $bound_lessons as an ACF to the recently created course with id: $id */
			update_field("bound_lessons", $bound_lessons, $id);
			
		}
		
	}

}

function json_migrate_save_lesson($lesson_id, $url_api) {

	$wpData = new wpData();
		
	/* get lesson data (including ACFs) */
	$url = $url_api."sfwd-lessons/".$lesson_id;
	
	$result = json_decode(file_get_contents($url), true);	
	
	/* call `parse_qs_lesson_step` with `content_raw` to create VC shortcodes */
	if (array_key_exists("content_raw", $result) and $result["content_raw"]) {
		$result["content"]["rendered"] = parse_qs_lesson_step($result["content_raw"], $url_api);
	}
	
	/* save lesson */
	$result_post_creation = $wpData->process_post_creation($result, [], "publish", $url_api, false);

	if($result_post_creation and $result_post_creation["id"]){

		/* save ACFs */
		if (array_key_exists("acf", $result) and is_array($result["acf"])) {
			
			foreach($result["acf"] as $acf_name => $acf_value) {
				if ($acf_name) {

					// if this is an array save sub fields accordingly
					// https://www.advancedcustomfields.com/resources/update_sub_field/
					if (is_array($acf_value)) {
						foreach ($acf_value as $index=>$sub_fields) {
							foreach ($sub_fields as $sub_fields_name=>$sub_fields_value) {
								// creates the father
								$the_index = $index+1;
								update_field($acf_name, $the_index, $result_post_creation["id"]);
								// and children
								update_sub_field( array(acf_find_key($acf_name), $the_index, acf_find_key($sub_fields_name)), $sub_fields_value, $result_post_creation["id"] );
							}
						}
					// if not save normal fields
					} else {
						update_field($acf_name, $acf_value, $result_post_creation["id"]);
					}

				}
			}

		}
		
		/* add lesson id to a new array $bound_lessons */
		 $bound_lessons[]=$result_post_creation["id"];
		
		return $result_post_creation["id"];

	}else{
		return null;
	}

}


// we will use our own js file

// remove the other wizard
function wpdocs_dequeue_wizard() {
	wp_dequeue_script( 'wizard-js' );
}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_wizard', 100 );

// load ours
function my_scripts_method_qs2() {
    wp_enqueue_script( 'wizard-js-qs2', dirname(plugin_dir_url())."/mu-plugins/json-migrate-qs2/js/wizard-qs2.js?ver=4.5.6", array('jquery'), '4.5.6' );
	wp_localize_script( 'wizard-js-qs2', 'jsonFile', dirname(plugin_dir_url())."/mu-plugins/json-migrate/js/sites.json" );
	wp_localize_script( 'wizard-js-qs2', 'adminUrl', admin_url() );
}
add_action( 'admin_init', 'my_scripts_method_qs2', 100 );


// Extract lessons, import image and create a new shortcode string
function parse_qs_lesson_step($text, $url_api) {

	$pattern = '\[(\[?)(qs_lesson_step)(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)';

	preg_match_all( '/'. $pattern .'/s', $text, $matches );

	$keys = array();
	$result = array();

	foreach($matches[0] as $key => $value) {

		// form pairs with & as separator
		$get = str_replace(" show_step=", "&show_step=" , $matches[3][$key] );
		$get = str_replace(" heading=", "&heading=" , $get );
		$get = str_replace(" image=", "&image=" , $get );

		parse_str($get, $attributes);

		// replace image="2232" from the original text and put the new image ID
		if ($attributes["image"]) {

			// get image code
			$image = str_replace(['"', "'"], '', $attributes["image"]);

			// get the original image source
			$media_res = json_decode(file_get_contents($url_api."media/".$image), true);
			if ($img_source = $media_res["media_details"]["sizes"]["full"]["source_url"]) {

				// import image and get the new ID
				$wpData = new wpData();
				$new_image = $wpData->uploadImage($img_source);
				if ($new_image) {

					// replace image="2232" from the original text and put the new image ID
					// 		example [qs_lesson_step show_step="true" heading="First, get to know the menu on the left. " image="2232"]whatever[/qs_lesson_step]
					$text = str_replace('image="'.$image.'"', 'image="'.$new_image.'"', $text);

				}
				
			}

		} 

	}
	
	return $text;

}

function acf_find_key($field_name) {
	switch($field_name) {
	case 'cta_button_group':	return 'field_565caa06f14a7';
	case 'button_title': 		return 'field_jm565caa67f14a8';
	case 'button_link': 		return 'field_565caa67f14a8';
	case 'site_cat_val': 		return 'field_sc565caa67f14a8';
	case 'button_color': 		return 'field_565caac7f14a9';
	case 'button_icon': 		return 'field_565cab37f14ab';
	default: return $field_name;
	}
}
